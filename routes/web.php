<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::resource('proyectos/proyecto','ProyectoController');

Route::resource('costos/materiaprima','MateriaPrimaController');

Route::resource('costos/prestaciones','PrestacionesController');

Route::resource('costos/embalaje','CostoEmbalajeController');

Route::resource('costos/materiales','OtrosMaterialesController');

Route::resource('costos/mantenimiento','MantenimientoController');

Route::resource('consumos/agua','ConsumoAguaController');

Route::resource('consumos/energia','ConsumoEnergiaController');

Route::resource('costos/manoobra','CostoManoObraDirectaController');

Route::resource('produccion/presupuesto','PresupuestoCostoProduccionController');

Route::resource('produccion/unitaria','CostoProduccionUnitarioController');

Route::resource('gastos/administracion','GastosAdministracionController');

Route::resource('gastos/generales','GastosGeneralesController');

Route::resource('gastos/oficina','GastosOficinaController');

Route::resource('gastos/ventas','GastosVentasController');

Route::resource('gastos/totales','TotalGastosAdministracionController');

Route::resource('capital/trabajo','CapitalTrabajoController');

Route::resource('ingresos','IngresosController');

Route::resource('flujo/caja','FlujoCajaController');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
