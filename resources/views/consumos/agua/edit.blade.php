@extends('layouts.admin')
@section('contenido')
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm6 col-xs-12">
			<h3>Agregar nuevo valor</h3>

			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li> {{$error}} </li>
						@endforeach
					</ul>
				</div>
			@endif

			{!!Form::model($agua, ['method' => 'PATCH', 'route' => ['agua.update', $agua->id_consumo_agua]])!!}			
				{{Form::token()}}				

				<div class="form-group">
					<label for="nomina">Concepto de {{$agua->concepto}}</label>
				</div>

				<div class="form-group">
				 	<label for="cantidad">Cantidad en metros cúbicos diaria</label>					
			      	<input id="msg" type="text" class="form-control" name="cantidad" value="{{$agua->cantidad}}" placeholder="Cantidad">
			    </div>

				<div class="form-group">
					<label for="costo_unitario">Costo unitario</label>					
			     	 <input id="msg" type="text" class="form-control" name="costo_unitario"  value="{{$agua->costo_unitario}}" placeholder="Costo unitario">
			    </div>

				<div class="form-group">
					<button class="btn btn-primary" type="submit">Guardar</button>

					<button class="btn btn-danger" type="reset">Cancelar</button>
				</div>	
			{!!Form::close()!!}
		</div>
	</div>
@endsection