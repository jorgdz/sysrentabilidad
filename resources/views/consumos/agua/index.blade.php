@extends('layouts.admin')
@section('contenido')
<div class="card mb-3">
    <div class="card-header">
          <h4>Consumo de agua</h4>          
     </div>
  	@include('consumos.agua.search')

        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Concepto</th>
                  <th>Cantidad</th>
                  <th>Consumo Anual</th>
                  <th>Costo unitario</th>
                  <th>Costo anual</th>
                  <th>Acción</th>
                </tr>
              </thead>

              <tfoot>
                    <tr>
                      <th></th>
                      <th></th>
                      <th></th>
                      <th></th>
                      <th>TOTAL COSTO UNITARIO</th>
                      @if($total->total2 < 1)
                        <th>0</th>
                      @else
                        <th>{{$total_agua->total1 / $total->total2}}</th>                     
                      @endif
                      <th></th>                       
                    </tr>
                </tfoot>

              <tbody>
              	@foreach($consumo_agua as $ca)
					        <tr>
    	                  <td>{{$ca->id_consumo_agua}}</td>
                        <td>{{$ca->concepto}}</td>
                        <td>{{$ca->cantidad}}</td>
                        <td>{{$ca->consumo_anual}}</td>
                        <td>{{$ca->costo_unitario}}</td>
                        <td>{{$ca->costo_anual}}</td>
                        <td>
              						<a href="{{URL::action('ConsumoAguaController@edit',$ca->id_consumo_agua)}}"><button class="btn btn-info">Agregar Valores</button></a>                                      						
	                      </td>
                	</tr>       
              	@endforeach

              </tbody>
            </table>
          </div>
          {{$consumo_agua->render()}}
        </div>
        <div class="card-footer small text-muted">Desarrollado por Jorge Diaz</div>
      </div>
@endsection