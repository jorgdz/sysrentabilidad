@extends('layouts.admin')
@section('contenido')
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm6 col-xs-12">
			<h3>Agregar nuevo valor</h3>

			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li> {{$error}} </li>
						@endforeach
					</ul>
				</div>
			@endif

			{!!Form::model($energia, ['method' => 'PATCH', 'route' => ['energia.update', $energia->id_consumo_energia]])!!}			
				{{Form::token()}}				

				<div class="form-group">
					<label for="nomina">Descripción de {{$energia->descripcion}}</label>
				</div>

				<div class="form-group">
				 	<label for="consumo_mensual">Consumo mensual en $</label>					
			      	<input id="msg" type="text" class="form-control" name="consumo_mensual" value="{{$energia->consumo_mensual}}" placeholder="Consumo mensual">
			    </div>

				<div class="form-group">
					<button class="btn btn-primary" type="submit">Guardar</button>

					<button class="btn btn-danger" type="reset">Cancelar</button>
				</div>	
			{!!Form::close()!!}
		</div>
	</div>
@endsection