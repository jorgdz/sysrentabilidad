@extends('layouts.admin')
@section('contenido')
<div class="card mb-3">
    <div class="card-header">
          <h4>Consumo de energía</h4>          
     </div>
  	@include('consumos.energia.search')

        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Concepto</th>
                  <th>Consumo mensual en $</th>
                  <th>Consumo Anual</th>
                  <th>Acción</th>
                </tr>
              </thead>

              <tfoot>
                    <tr>
                      <th></th>
                      <th></th>
                      <th>TOTAL COSTO UNITARIO</th>
                      @if($total->total2 < 1)
                          <th>0</th>
                      @else
                        <th>{{$total_energia->total1 / $total->total2}}</th>                     
                      @endif
                      <th></th>                       
                    </tr>
                </tfoot>

              <tbody>
              	@foreach($consumo_energia as $ce)
					        <tr>
    	                  <td>{{$ce->id_consumo_energia}}</td>
                        <td>{{$ce->descripcion}}</td>
                        <td>{{$ce->consumo_mensual}}</td>
                        <td>{{$ce->consumo_anual}}</td>
                        <td>
              						<a href="{{URL::action('ConsumoEnergiaController@edit',$ce->id_consumo_energia)}}"><button class="btn btn-info">Agregar Valores</button></a>                                      						
	                      </td>
                	</tr>       
              	@endforeach

              </tbody>
            </table>
          </div>
          {{$consumo_energia->render()}}
        </div>
        <div class="card-footer small text-muted">Desarrollado por Jorge Diaz</div>
      </div>
@endsection