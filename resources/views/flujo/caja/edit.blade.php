@extends('layouts.admin')
@section('contenido')
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm6 col-xs-12">
			<h3>Actualizar Valores</h3>

			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li> {{$error}} </li>
						@endforeach
					</ul>
				</div>
			@endif

			{!!Form::model($flujo, ['method' => 'PATCH', 'route' => ['caja.update', $flujo->id_flujo_caja]])!!}			
				{{Form::token()}}				

				<div class="form-group">
					<label for="nomina">Ingreso de {{$flujo->ingresos}}</label>
				</div>
				@if($flujo->ingresos == 'Préstamo')
					<div class="form-group">
					 	<label for="inicial">Valor inicial</label>					
				      	<input id="msg" type="text" class="form-control" name="inicial" value="{{$flujo->inicial}}" placeholder="Valor inicial">
				    </div>
				@else
					<div class="form-group">
						<label for="anio1">Año 1</label>					
				     	 <input id="msg" type="text" class="form-control" name="anio1"  value="{{$flujo->anio1}}" placeholder="Año 1">
				    </div>

				    <div class="form-group">
						<label for="anio2">Año 2</label>					
				     	 <input id="msg" type="text" class="form-control" name="anio2"  value="{{$flujo->anio2}}" placeholder="Año 2">
				    </div>
					
					<div class="form-group">
						<label for="anio3">Año 3</label>					
				     	 <input id="msg" type="text" class="form-control" name="anio3"  value="{{$flujo->anio3}}" placeholder="Año 3">
				    </div>
					
					<div class="form-group">
						<label for="anio4">Año 4</label>					
				     	 <input id="msg" type="text" class="form-control" name="anio4"  value="{{$flujo->anio4}}" placeholder="Año 4">
				    </div>
					
					<div class="form-group">
						<label for="anio5">Año 5</label>					
				     	 <input id="msg" type="text" class="form-control" name="anio5"  value="{{$flujo->anio5}}" placeholder="Año 5">
				    </div>
				@endif

			    
				<div class="form-group">
					<button class="btn btn-primary" type="submit">Guardar</button>

					<button class="btn btn-danger" type="reset">Cancelar</button>
				</div>	
			{!!Form::close()!!}
		</div>
	</div>
@endsection