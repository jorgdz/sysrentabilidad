{!! Form::open(array('url' =>'flujo/caja' , 'method'=>'GET', 'autocomplete'=>'off', 'role'=>'search')) !!}
	
	<div class="card-header">
		<div class="input-group">
			<select name="id_proyecto" class="form-control" id="">
				@foreach($proyecto as $p)
					<option value="{{$p->id_proyecto}}">{{$p->nombre_proyecto}}</option>
				@endforeach
			</select>
			<span class="input-group-btn">
				<button type="submit" class="btn btn-success">Seleccionar proyecto</button>
			</span>
		</div>
	</div>

{{Form::close()}}