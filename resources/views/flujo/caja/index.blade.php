@extends('layouts.admin')
@section('contenido')
  <div class="card mb-3">
    <div class="card-header">
        <h4>Flujo de caja</h4>          
    </div>
    @include('flujo.caja.search')

        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Ingresos</th>
                  <th>Inicial</th>
                  <th>Año 1</th>
                  <th>Año 2</th>
                  <th>Año 3</th>
                  <th>Año 4</th>
                  <th>Año 5</th>
                  <th>Acción</th>
                </tr>
              </thead>

              <tfoot>
                  <tr>
                    <th></th>
                    <th>EGRESOS TOTALES</th>
                    <th></th>
                    <th>{{$fc_total_egreso_anio->total1}}</th>
                    <th>{{$fc_total_egreso_anio->total2}}</th>
                    <th>{{$fc_total_egreso_anio->total3}}</th>
                    <th>{{$fc_total_egreso_anio->total4}}</th>
                    <th>{{$fc_total_egreso_anio->total5}}</th>                  
                    <th></th>
                  </tr>
                  <tr>
                    <th></th>
                    <th>Inversión</th>
                    <th>{{$fc_inversion->total_inicial * -1}}</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                  </tr>
                  <tr>
                      <th></th>
                      <th>Valor Actual Neto (VNA)</th>
                      <th style="color: crimson;">{{ $van }}</th>
                  </tr>
                  <tr>
                    <th></th>
                    @if($van < 0)
                      <th>El VAN es negativo por lo tanto el proyecto no es rentable</th>
                    @elseif( $van == 0)
                      <th>El van tiene un valor neutro</th>
                    @else
                      <th>El van es mayor a 0 por lo tanto es rentable</th>
                    @endif
                  </tr>
                  <tr>
                    <th></th>
                    <th>El van es calculado con una tasa de interés del 9.80%</th>
                  </tr>           
              </tfoot>

              <tbody>

                @foreach($flujo as $f)
                  <tr>
                    <td>{{$f->id_flujo_caja}}</td>
                    <td>{{$f->ingresos}}</td>
                    <td>{{$f->inicial}}</td>                       
                    <td>{{$f->anio1}}</td>                       
                    <td>{{$f->anio2}}</td>                       
                    <td>{{$f->anio3}}</td>                       
                    <td>{{$f->anio4}}</td>                       
                    <td>{{$f->anio5}}</td>  
                    <td>
                      @if($f->ingresos == 'Préstamo' or $f->ingresos == 'Depreciación' or $f->ingresos == 'Amortización de intangibles' or $f->ingresos == 'Gastos Financieros')
                        <a href="{{URL::action('FlujoCajaController@edit',$f->id_flujo_caja)}}"><button class="btn btn-info">Actualizar</button></a>                                              
                      @endif
                    </td>                     
                  </tr>       
                @endforeach

              </tbody>
            </table>
          </div>
          {{$flujo->render()}}
        </div>
        <div class="card-footer small text-muted">Desarrollado por Jorge Diaz</div>
    </div>
@endsection