@extends('layouts.admin')
@section('contenido')
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm6 col-xs-12">
			<h3>Editar proyecto</h3>

			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li> {{$error}} </li>
						@endforeach
					</ul>
				</div>
			@endif

			{!!Form::model($proyecto,['method' => 'PATCH', 'route' => ['proyecto.update', $proyecto->id_proyecto]])!!}			
				{{Form::token()}}

				<div class="form-group">
					<label for="nombre">Nombre del proyecto</label>
					<input type="text" name="nombre_proyecto" class="form-control" value ="{{$proyecto->nombre_proyecto}}" placeholder="Nombre">
				</div>
				<div class="form-group">
					<label for="descripcion">Descripcion</label>
					<input type="text" name="descripcion" class="form-control" value="{{$proyecto->descripcion}}" placeholder="Descripcion">
				</div>
				
				<div class="form-group">
					<button class="btn btn-primary" type="submit">Guardar</button>

					<button class="btn btn-danger" type="reset">Cancelar</button>
				</div>	
			{!!Form::close()!!}
		</div>
	</div>
@endsection