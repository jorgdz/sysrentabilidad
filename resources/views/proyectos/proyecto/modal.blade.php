<div class="modal fade model-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-delete-{{$project->id_proyecto}}">
	
	{{Form::Open(array('action' => array('ProyectoController@destroy', $project->id_proyecto), 'method' => 'delete'))}}
	
		<div class="modal-dialog">
			<div class="modal-content">
				<!--Cabecera del modal-->
				<div class="modal-header">
					<button type="button" class="close" data-dismiss ="modal" aria-label="Close">
						<span aria-hidden="true"></span>
					</button>
					<h4 class="modal-title">Eliminar Proyecto</h4>
				</div>
				<!--Cuerpo del modal-->
				<div class="modal-body">
					<p>Realmente desea borrar el proyecto "{{$project->nombre_proyecto}}" </p>
				</div>
				<!--Pié de página del modal-->
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-primary">Aceptar</button>
				</div>
			</div>
		</div>
	{{Form::close()}}
</div>


