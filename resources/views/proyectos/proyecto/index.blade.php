@extends('layouts.admin')
@section('contenido')
<div class="card mb-3">
     <div class="card-header">
          <i class="fa fa-table"></i>
          <a href="proyecto/create" class="btn btn-primary" type ="submit" name="btn_nuevo">Nuevo proyecto</a>
     </div>
     	@include('proyectos.proyecto.search')

        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Nombre del proyecto</th>
                  <th>Descripción</th>
                  <th>Acciones</th>
                </tr>
              </thead>
              <tbody>
              	@foreach($proyecto as $project)
					         <tr>
    	                  <td>{{$project->id_proyecto}}</td>
    	                  <td>{{$project->nombre_proyecto}}</td>
    	                  <td>{{$project->descripcion}}</td>
    	                  <td>
              						<a href="{{URL::action('ProyectoController@edit',$project->id_proyecto)}}"><button class="btn btn-info">Editar</button></a>
                          
              						<a href="#" data-target="#modal-delete-{{$project->id_proyecto}}" data-toggle="modal"><button class="btn btn-danger">Borrar</button></a>
	                     </td>
                	</tr> 
                  @include('proyectos.proyecto.modal')      
              	@endforeach                 
              </tbody>
            </table>
          </div>
          {{$proyecto->render()}}
        </div>
        <div class="card-footer small text-muted">Desarrollado por Jorge Diaz</div>
      </div>
@endsection