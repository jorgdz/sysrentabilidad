{!! Form::open(array('url' =>'proyectos/proyecto' , 'method'=>'GET', 'autocomplete'=>'off', 'role'=>'search')) !!}
	
	<div class="card-header">
		<div class="input-group">
			<input type="text" class="form-control" name="search" placeholder="Buscar proyecto por nombre" value="{{$search}}">
			<span class="input-group-btn">
				<button type="submit" class="btn btn-success">Buscar</button>
			</span>
		</div>
	</div>

{{Form::close()}}