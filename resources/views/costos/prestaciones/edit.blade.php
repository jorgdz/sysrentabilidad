@extends('layouts.admin')
@section('contenido')
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm6 col-xs-12">
			<h3>Agregar nuevo valor</h3>

			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li> {{$error}} </li>
						@endforeach
					</ul>
				</div>
			@endif

			{!!Form::model($prestacion, ['method' => 'PATCH', 'route' => ['prestaciones.update', $prestacion->id_prestaciones]])!!}			
				{{Form::token()}}				

				<div class="form-group">
					<label for="nomina">Nómina de {{$prestacion->nomina}}</label>
				</div>

				<div class="form-group">
				 	<label for="numero">Número</label>					
			      <input id="msg" type="number" class="form-control" name="numero" value="{{$prestacion->numero}}" placeholder="Número">
			    </div>

				<div class="form-group">
					<label for="sueldo_mensual">Sueldo mensual</label>					
			     	 <input id="msg" type="text" class="form-control" name="sueldo_mensual"  value="{{$prestacion->sueldo_mensual}}" placeholder="Sueldo mensual">
			    </div>

			    <div class="form-group">
					<label for="horas_suplementarias">Horas suplementarias</label>					
			     	 <input id="msg" type="text" class="form-control" name="horas_suplementarias"  value="{{$prestacion->horas_suplementarias}}" placeholder="Horas suplementarias">
			    </div>
			    
			    <div class="form-group">
					<label for="treceavo_sueldo">13º Sueldo</label>					
			     	 <input id="msg" type="text" class="form-control" name="treceavo_sueldo"  value="{{$prestacion->treceavo_sueldo}}" placeholder="3ºvo. Sueldo">
			    </div>
			    
			    <div class="form-group">
					<label for="catorceavo_sueldo">14º Sueldo</label>					
			     	 <input id="msg" type="text" class="form-control" name="catorceavo_sueldo"  value="{{$prestacion->catorceavo_sueldo}}" placeholder="14º Sueldo">
			    </div>
			    
			    <div class="form-group">
					<label for="fondo_reserva">Fondo de reserva</label>					
			     	 <input id="msg" type="text" class="form-control" name="fondo_reserva"  value="{{$prestacion->fondo_reserva}}" placeholder="Fondo de reserva">
			    </div>
			    
			    <div class="form-group">
					<label for="vacaciones">Vacaciones</label>					
			     	 <input id="msg" type="text" class="form-control" name="vacaciones"  value="{{$prestacion->vacaciones}}" placeholder="Vacaciones">
			    </div>
				
				<div class="form-group">
					<button class="btn btn-primary" type="submit">Guardar</button>

					<button class="btn btn-danger" type="reset">Cancelar</button>
				</div>	
			{!!Form::close()!!}
		</div>
	</div>
@endsection