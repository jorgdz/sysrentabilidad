@extends('layouts.admin')
@section('contenido')
<div class="card mb-3">
    <div class="card-header">
          <h4>Prestaciones</h4>          
     </div>
  	@include('costos.prestaciones.search')

        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Nómina</th>
                  <th>Número</th>
                  <th>Sueldo mensual</th>
                  <th>Horas suplementarias</th>
                  <th>Sueldo anual</th>
                  <th>13º Sueldo</th>
                  <th>14º Sueldo</th>
                  <th>Fondo de reserva</th>
                  <th>Vacaciones</th>
                  <th>Aportes al IESS</th>
                  <th>Total de prestaciones en 1º año</th>
                  <th>Acción</th>
                </tr>
              </thead>

              <tfoot>
                    <tr>
                      <th></th>
                      <th>TOTAL</th>
                      <th>{{$totales->total1}}</th>
                      <th>{{$totales->total2}}</th>
                      <th>{{$totales->total3}}</th>
                      <th>{{$totales->total4}}</th>
                      <th>{{$totales->total5}}</th>
                      <th>{{$totales->total6}}</th>
                      <th>{{$totales->total7}}</th>
                      <th>{{$totales->total8}}</th>
                      <th>{{$totales->total9}}</th>
                      <th>{{$totales->total10}}</th>
                      <th></th>                       
                    </tr>
                </tfoot>

              <tbody>
              	@foreach($prestacion as $p)
					        <tr>
    	                  <td>{{$p->id_prestaciones}}</td>
    	                  <td>{{$p->nomina}}</td>
    	                  <td>{{$p->numero}}</td>
                        <td>{{$p->sueldo_mensual}}</td>
                        <td>{{$p->horas_suplementarias}}</td>
                        <td>{{$p->sueldo_anual}}</td>
                        <td>{{$p->treceavo_sueldo}}</td>
                        <td>{{$p->catorceavo_sueldo}}</td>
                        <td>{{$p->fondo_reserva}}</td>
                        <td>{{$p->vacaciones}}</td>
                        <td>{{$p->aporte_iess}}</td>
                        <td>{{$p->total_prestaciones_primer_aniol}}</td>
                        <td>
              						<a href="{{URL::action('PrestacionesController@edit',$p->id_prestaciones)}}"><button class="btn btn-info">Agregar Valores</button></a>                                      						
	                      </td>
                	</tr>       
              	@endforeach

              </tbody>
            </table>
          </div>
          {{$prestacion->render()}}
        </div>
        <div class="card-footer small text-muted">Desarrollado por Jorge Diaz</div>
      </div>
@endsection