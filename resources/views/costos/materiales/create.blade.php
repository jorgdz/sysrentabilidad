@extends('layouts.admin')
@section('contenido')
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm6 col-xs-12">
			<h3>Registrar nuevo material</h3>

			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li> {{$error}} </li>
						@endforeach
					</ul>
				</div>
			@endif

			{!!Form::open(array('url' => 'costos/materiales', 'method' => 'POST', 'autocomplete' => 'off'))!!}			
				{{Form::token()}}
				
				<div class="form-group">
					<label>Proyecto</label>
					<select name="id_proyecto" class="form-control" id="">
						@foreach($proyecto as $p)
								<option value="{{$p->id_proyecto}}">{{$p->nombre_proyecto}}</option>
						@endforeach
					</select>
				</div>				

				<div class="form-group">
					<label for="concepto">Concepto</label>
					<input type="text" name="concepto" class="form-control" placeholder="Concepto">
				</div>

				<div class="form-group">
				  <label for="cantidad">Cantidad</label>					
			      <span class="input-group-addon">$</span>
			      <input id="msg" type="text" class="form-control" name="cantidad" placeholder="Cantidad">
			    </div>

				<div class="form-group">
				  <label for="unidad">Unidad</label>					
			      <input id="msg" type="text" class="form-control" name="unidad" placeholder="Unidad">
			    </div>

				<div class="form-group">
				  <label for="costo_unitario">Costo Unitario</label>					
			      <input id="msg" type="text" class="form-control" name="costo_unitario" placeholder="Costo unitario">
			    </div>			    
				
				<div class="form-group">
					<button class="btn btn-primary" type="submit">Guardar</button>

					<button class="btn btn-danger" type="reset">Cancelar</button>
				</div>	
			{!!Form::close()!!}
		</div>
	</div>
@endsection