@extends('layouts.admin')
@section('contenido')
<div class="card mb-3">
     <div class="card-header">
          <i class="fa fa-table"></i>          
          <a href="materiales/create" class="btn btn-primary" type ="submit" name="btn_nuevo">Nuevos Materiales</a>
     </div>
  	@include('costos.materiales.search')

        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Concepto</th>
                  <th>Cantidad</th>
                  <th>Unidad</th>
                  <th>Costo unitario</th>
                  <th>Costos anuales</th>
                  <th>Acción</th>
                </tr>
              </thead>

              <tfoot>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th>Total</th>
                    <th><h4 id="tt">{{ $total->total1 }}</h4></th>  
                    <th></th>                   
                </tr>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th>Costo Unitario</th>
                    @if ($total->total1 < 1)
                      <th><h4 id="">0</h4></th>  
                    @else                      
                      <th><h4 id="">{{ round($total->total1 / $totalmp->total1, 3) }}</h4></th>  
                    @endif
                      <th></th>                       
                </tr>
              </tfoot>

              <tbody>
              	@foreach($otros_materiales as $om)
					        <tr>
    	                  <td>{{$om->id_otros_materiales}}</td>
    	                  <td>{{$om->concepto}}</td>
                        <td>{{$om->cantidad}}</td>
                        <td>{{$om->unidad}}</td>
                        <td>{{$om->costo_unitario}}</td>
                        <td>{{$om->costo_anual}}</td>
    	                  <td>
              						<a href="{{URL::action('OtrosMaterialesController@edit',$om->id_otros_materiales)}}"><button class="btn btn-info">Editar</button></a>                            						
	                      </td>
                	</tr> 
              	@endforeach

              </tbody>
            </table>
          </div>
          {{$otros_materiales->render()}}
        </div>
        <div class="card-footer small text-muted">Desarrollado por Jorge Diaz</div>
      </div>
@endsection