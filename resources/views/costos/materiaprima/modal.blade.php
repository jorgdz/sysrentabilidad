<div class="modal fade model-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-delete-{{$mp->id_materia_prima}}">
	
	{{Form::Open(array('action' => array('MateriaPrimaController@destroy', $mp->id_materia_prima), 'method' => 'delete'))}}
	
		<div class="modal-dialog">
			<div class="modal-content">
				<!--Cabecera del modal-->
				<div class="modal-header">
					<button type="button" class="close" data-dismiss ="modal" aria-label="Close">
						<span aria-hidden="true"></span>
					</button>
					<h4 class="modal-title">Eliminar Materia prima</h4>
				</div>
				<!--Cuerpo del modal-->
				<div class="modal-body">
					<p>Realmente desea borrar esta materia prima "{{$mp->descripcion}}" </p>
				</div>
				<!--Pié de página del modal-->
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-primary">Aceptar</button>
				</div>
			</div>
		</div>
	{{Form::close()}}
</div>


