@extends('layouts.admin')
@section('contenido')
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm6 col-xs-12">
			<h3>Editar materia prima</h3>

			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li> {{$error}} </li>
						@endforeach
					</ul>
				</div>
			@endif

			{!!Form::model($materiap,['method' => 'PATCH', 'route' => ['materiaprima.update', $materiap->id_materia_prima]])!!}			
				{{Form::token()}}
				<div class="form-group">
						<label>Proyectos</label>
						<select name="id_proyecto" class="form-control" id="">
							@foreach($proyecto as $p)
								@if($p->id_proyecto == $materiap->id_proyecto)
									<option value="{{$p->id_proyecto}}" selected>{{$p->nombre_proyecto}}</option>
								@else
									<option value="{{$p->id_proyecto}}">{{$p->nombre_proyecto}}</option>
								@endif
							@endforeach
						</select>
				</div>

				<div class="form-group">
					<label for="descripcion">Descripcion</label>
					<input type="text" name="descripcion" class="form-control" value="{{$materiap->descripcion}}" placeholder="Descripcion">
				</div>

				<div class="form-group">
				 	<label for="descripcion">Costo Unitario</label>					
			      <input id="msg" type="text" class="form-control" name="costo_unitario" value="{{$materiap->costo_unitario}}" placeholder="Costo unitario">
			    </div>

				<div class="form-group">
					<label for="descripcion">Demanda Anual</label>					
			     	 <input id="msg" type="text" class="form-control" name="demanda_anual"  value="{{$materiap->demanda_anual}}"" placeholder="Demanda anual">
			    </div>
			    
				<div class="form-group">
					<button class="btn btn-primary" type="submit">Guardar</button>

					<button class="btn btn-danger" type="reset">Cancelar</button>
				</div>	
			{!!Form::close()!!}
		</div>
	</div>
@endsection