@extends('layouts.admin')
@section('contenido')
<div class="card mb-3">
     <div class="card-header">
          <i class="fa fa-table"></i>          
          <a href="materiaprima/create" class="btn btn-primary" type ="submit" name="btn_nuevo">Nueva Materia Prima</a>
     </div>
  	@include('costos.materiaprima.search')

        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Nombre del proyecto</th>
                  <th>Descripción de materia prima</th>
                  <th>Costo Unitario</th>
                  <th>Demanda anual</th>
                  <th>Costo total anual</th>
                  <th>Acciones</th>
                </tr>
              </thead>

              <tfoot>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th>Total</th>
                    <th><h4 id="ttd">{{ $total->total1 }}</h4></th>  
                    <th><h4 id="ttca">{{ $total->total2 }}</h4></th> 
                    <th></th>                   
                    </tr>
                    <tr>
                      <th></th>
                      <th></th>
                      <th></th>
                      <th></th>
                      <th>Costo Unitario promedio</th>
                      @if ($total->total1 < 1)
                        <th><h4 id="costo_unitario_promedio">0</h4></th>  
                      @else                      
                        <th><h4 id="costo_unitario_promedio">{{ round($total->total2 / $total->total1, 3) }}</h4></th>  
                      @endif
                      <th></th>                       
                    </tr>
                </tfoot>

              <tbody>
              	@foreach($materiap as $mp)
					        <tr>
    	                  <td>{{$mp->id_materia_prima}}</td>
    	                  <td>{{$mp->nombre_proyecto}}</td>
    	                  <td>{{$mp->descripcion}}</td>
                        <td>{{$mp->costo_unitario}}</td>
                        <td>{{$mp->demanda_anual}}</td>
                        <td>{{$mp->costo_total_anual}}</td>
    	                  <td>
              						<a href="{{URL::action('MateriaPrimaController@edit',$mp->id_materia_prima)}}"><button class="btn btn-info">Editar</button></a>
                          
              						<a href="#" data-target="#modal-delete-{{$mp->id_materia_prima}}" data-toggle="modal"><button class="btn btn-danger">Borrar</button></a>
	                      </td>
                	</tr> 
                  @include('costos.materiaprima.modal')      
              	@endforeach

              </tbody>
            </table>
          </div>
          {{$materiap->render()}}
        </div>
        <div class="card-footer small text-muted">Desarrollado por Jorge Diaz</div>
      </div>
@endsection