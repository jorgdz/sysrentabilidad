@extends('layouts.admin')
@section('contenido')
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm6 col-xs-12">
			<h3>Registrar nueva mano de obra directa</h3>

			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li> {{$error}} </li>
						@endforeach
					</ul>
				</div>
			@endif

			{!!Form::open(array('url' => 'costos/manoobra', 'method' => 'POST', 'autocomplete' => 'off'))!!}			
				{{Form::token()}}
				
				<div class="form-group">
					<label>Proyecto</label>
					<select name="id_proyecto" class="form-control" id="">
						@foreach($proyecto as $p)
								<option value="{{$p->id_proyecto}}">{{$p->nombre_proyecto}}</option>
						@endforeach
					</select>
				</div>				

				<div class="form-group">
					<label for="personal">Personal</label>
					<input type="text" name="personal" class="form-control" placeholder="Personal">
				</div>


				<div class="form-group">
				  <label for="cantidad">Cantidad</label>					
			      <input id="msg" type="text" class="form-control" name="cantidad" placeholder="Cantidad">
			    </div>
				
				<div class="form-group">
				  <label for="remuneracion">Remuneracion mensual</label>					
			      <span class="input-group-addon">$</span>
			      <input id="msg" type="text" class="form-control" name="remuneracion" placeholder="Remuneracion mensual">
			    </div>

				<div class="form-group">
					<button class="btn btn-primary" type="submit">Guardar</button>

					<button class="btn btn-danger" type="reset">Cancelar</button>
				</div>	
			{!!Form::close()!!}
		</div>
	</div>
@endsection