@extends('layouts.admin')
@section('contenido')
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm6 col-xs-12">
			<h3>Editar mano de obra</h3>

			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li> {{$error}} </li>
						@endforeach
					</ul>
				</div>
			@endif

			{!!Form::model($costomanoobra,['method' => 'PATCH', 'route' => ['manoobra.update', $costomanoobra->id_costo_mano_obra_directa]])!!}			
				{{Form::token()}}
				<div class="form-group">
					<label>Proyectos</label>
					<select name="id_proyecto" class="form-control" id="">
						@foreach($proyecto as $p)
							@if($p->id_proyecto == $costomanoobra->id_proyecto)
								<option value="{{$p->id_proyecto}}" selected>{{$p->nombre_proyecto}}</option>
							@else
								<option value="{{$p->id_proyecto}}">{{$p->nombre_proyecto}}</option>
							@endif
						@endforeach
					</select>
				</div>
				
				<div class="form-group">
					<label for="personal">Personal</label>
					<input type="text" name="personal" class="form-control" placeholder="Personal" value="{{$costomanoobra->personal}}">
				</div>


				<div class="form-group">
				  <label for="cantidad">Cantidad</label>					
			      <input id="msg" type="text" class="form-control" name="cantidad" placeholder="Cantidad" value="{{$costomanoobra->cantidad}}">
			    </div>
				
				<div class="form-group">
				  <label for="remuneracion">Remuneracion mensual</label>					
			      <span class="input-group-addon">$</span>
			      <input id="msg" type="text" class="form-control" name="remuneracion" placeholder="Remuneracion mensual" value="{{$costomanoobra->remuneracion}}">
			    </div>
			    
				<div class="form-group">
					<button class="btn btn-primary" type="submit">Guardar</button>

					<button class="btn btn-danger" type="reset">Cancelar</button>
				</div>	
			{!!Form::close()!!}
		</div>
	</div>
@endsection