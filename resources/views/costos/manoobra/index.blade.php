@extends('layouts.admin')
@section('contenido')
<div class="card mb-3">
     <div class="card-header">
          <i class="fa fa-table"></i>          
          <a href="manoobra/create" class="btn btn-primary" type ="submit" name="btn_nuevo">Nueva mano de obra</a>
     </div>
  	@include('costos.manoobra.search')

        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Personal</th>
                  <th>Cantidad</th>
                  <th>Remuneración mensual</th>
                  <th>Monto total</th>
                  <th>Acción</th>
                </tr>
              </thead>

              <tfoot>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th>Total</th>
                    <th><h4 id="tt">{{ $total_mano_obra->total1 }}</h4></th>  
                    <th></th>                   
                </tr>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th>Costo Unitario</th>
                    @if ($totales_materia_prima->total2 < 1)
                      <th><h4 id="">0</h4></th>  
                    @else                      
                      <th><h4 id="">{{ round($total_mano_obra->total1 / $totales_materia_prima->total2, 3) }}</h4></th>  
                    @endif
                      <th></th>                       
                </tr>
              </tfoot>

              <tbody>
              	@foreach($costomanoobra as $c)
					        <tr>
    	                  <td>{{$c->id_costo_mano_obra_directa}}</td>
    	                  <td>{{$c->personal}}</td>
    	                  <td>{{$c->cantidad}}</td>
                        <td>{{$c->remuneracion}}</td>
                        <td>{{$c->monto_total}}</td>
    	                  <td>
              						<a href="{{URL::action('CostoManoObraDirectaController@edit',$c->id_costo_mano_obra_directa)}}"><button class="btn btn-info">Editar</button></a>                            						
	                      </td>
                	</tr> 
              	@endforeach

              </tbody>
            </table>
          </div>
          {{$costomanoobra->render()}}
        </div>
        <div class="card-footer small text-muted">Desarrollado por Jorge Diaz</div>
      </div>
@endsection