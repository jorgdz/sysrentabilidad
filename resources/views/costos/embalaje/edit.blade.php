@extends('layouts.admin')
@section('contenido')
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm6 col-xs-12">
			<h3>Editar costo de embalaje</h3>

			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li> {{$error}} </li>
						@endforeach
					</ul>
				</div>
			@endif

			{!!Form::model($costoembalaje,['method' => 'PATCH', 'route' => ['embalaje.update', $costoembalaje->id_costo_embalaje]])!!}			
				{{Form::token()}}
				<div class="form-group">
						<label>Proyectos</label>
						<select name="id_proyecto" class="form-control" id="">
							@foreach($proyecto as $p)
								@if($p->id_proyecto == $costoembalaje->id_proyecto)
									<option value="{{$p->id_proyecto}}" selected>{{$p->nombre_proyecto}}</option>
								@else
									<option value="{{$p->id_proyecto}}">{{$p->nombre_proyecto}}</option>
								@endif
							@endforeach
						</select>
				</div>

				<div class="form-group">
					<label for="descripcion">Descripcion</label>
					<input type="text" name="descripcion" class="form-control" value="{{$costoembalaje->descripcion}}" placeholder="Descripcion">
				</div>

				<div class="form-group">
				 	<label for="costo_unitario">Costo Unitario</label>					
			      <input id="msg" type="text" class="form-control" name="costo_unitario" value="{{$costoembalaje->costo_unitario}}" placeholder="Costo unitario">
			    </div>

				<div class="form-group">
					<label for="cantidad">Cantidad</label>					
			     	 <input id="msg" type="text" class="form-control" name="cantidad"  value="{{$costoembalaje->cantidad}}"" placeholder="Cantidad">
			    </div>
			    
				<div class="form-group">
					<button class="btn btn-primary" type="submit">Guardar</button>

					<button class="btn btn-danger" type="reset">Cancelar</button>
				</div>	
			{!!Form::close()!!}
		</div>
	</div>
@endsection