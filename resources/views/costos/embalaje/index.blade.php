@extends('layouts.admin')
@section('contenido')
<div class="card mb-3">
     <div class="card-header">
          <i class="fa fa-table"></i>          
          <a href="embalaje/create" class="btn btn-primary" type ="submit" name="btn_nuevo">Nuevos Costos de Embalaje</a>
     </div>
  	@include('costos.embalaje.search')

        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Descripción</th>
                  <th>Costo unitario</th>
                  <th>Cantidad</th>
                  <th>Costos anuales</th>
                  <th>Acción</th>
                </tr>
              </thead>

              <tfoot>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th>Total</th>
                    <th><h4 id="tt">{{ $total->total1 }}</h4></th>  
                    <th></th>                   
                </tr>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th>Costo Unitario</th>
                    @if ($total->total1 < 1)
                      <th><h4 id="">0</h4></th>  
                    @else                      
                      <th><h4 id="">{{ round($total->total1 / $totalmp->total1, 3) }}</h4></th>  
                    @endif
                      <th></th>                       
                </tr>
              </tfoot>

              <tbody>
              	@foreach($costoembalaje as $ce)
					        <tr>
    	                  <td>{{$ce->id_costo_embalaje}}</td>
    	                  <td>{{$ce->descripcion}}</td>
    	                  <td>{{$ce->costo_unitario}}</td>
                        <td>{{$ce->cantidad}}</td>
                        <td>{{$ce->costo_anuales}}</td>
    	                  <td>
              						<a href="{{URL::action('CostoEmbalajeController@edit',$ce->id_costo_embalaje)}}"><button class="btn btn-info">Editar</button></a>                            						
	                      </td>
                	</tr> 
              	@endforeach

              </tbody>
            </table>
          </div>
          {{$costoembalaje->render()}}
        </div>
        <div class="card-footer small text-muted">Desarrollado por Jorge Diaz</div>
      </div>
@endsection