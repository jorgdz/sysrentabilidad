@extends('layouts.admin')
@section('contenido')
<div class="card mb-3">
    <div class="card-header">
          <h4>Capital de trabajo</h4>          
     </div>
  	@include('capital.trabajo.search')

        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Capital de trabajo</th>
                  <th>Valor</th>
                </tr>
              </thead>

              <tfoot>
                    <tr>
                      <th></th>
                      <th>Total</th>
                      @if($total->total < 1)
                        <th>0</th>
                      @else
                        <th>{{$total->total}}</th>
                      @endif
                    </tr>
                </tfoot>

              <tbody>
              	@foreach($capital as $ct)
					        <tr>
                        <td>{{$ct->id_capital_trabajo}}</td>
                        <td>{{$ct->concepto}}</td>
                        <td>{{$ct->valor}}</td>                        
                	</tr>       
              	@endforeach

              </tbody>
            </table>
          </div>
          {{$capital->render()}}
        </div>
        <div class="card-footer small text-muted">Desarrollado por Jorge Diaz</div>
      </div>
@endsection