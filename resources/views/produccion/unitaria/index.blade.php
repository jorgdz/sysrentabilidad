@extends('layouts.admin')
@section('contenido')
<div class="card mb-3">
    <div class="card-header">
          <h4>Costo de producción unitario</h4>          
     </div>
  	@include('produccion.unitaria.search')

        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Conceptos</th>
                  <th>Valor Unitario</th>
                </tr>
              </thead>

              <tfoot>
                    <tr>
                      <th></th>
                      <th>Total</th>
                      @if($total->total < 1)
                        <th>0</th>
                      @else
                        <th>{{$total->total}}</th>
                      @endif
                    </tr>
                </tfoot>

              <tbody>
              	@foreach($costoprodu as $c)
					        <tr>
                        <td>{{$c->id_costo_produccion_unitario}}</td>
                        <td>{{$c->conceptos}}</td>
    	                  <td>{{$c->valor_unitario}}</td>                       
                	</tr>       
              	@endforeach

              </tbody>
            </table>
          </div>
          {{$costoprodu->render()}}
        </div>
        <div class="card-footer small text-muted">Desarrollado por Jorge Diaz</div>
      </div>
@endsection