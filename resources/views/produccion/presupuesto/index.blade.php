@extends('layouts.admin')
@section('contenido')
<div class="card mb-3">
    <div class="card-header">
          <h4>Presupuesto de costo de producción</h4>          
     </div>
  	@include('produccion.presupuesto.search')

        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Concepto</th>
                  <th>Valor Anual</th>
                </tr>
              </thead>

              <tfoot>
                    <tr>
                      <th></th>
                      <th>Imprevistos 5 %</th>
                      @if($totales_presupuesto->total1 < 1)
                        <th>0</th>
                      @else
                        <th>{{$totales_presupuesto->total1 * 0.05}}</th>                     
                      @endif                       
                    </tr>

                    <tr>
                      <th></th>
                      <th>Total</th>
                      @if($totales_presupuesto->total1 < 1)
                        <th>0</th>
                      @else
                        <th>{{$totales_presupuesto->total1 + ($totales_presupuesto->total1 * 0.05)}}</th>
                      @endif
                    </tr>
                </tfoot>

              <tbody>
              	@foreach($presupuesto as $p)
					        <tr>
                        <td>{{$p->id_presupuesto_costo_produccion}}</td>
                        <td>{{$p->concepto}}</td>
    	                  <td>{{$p->valor_anual}}</td>                       
                	</tr>       
              	@endforeach

              </tbody>
            </table>
          </div>
          {{$presupuesto->render()}}
        </div>
        <div class="card-footer small text-muted">Desarrollado por Jorge Diaz</div>
      </div>
@endsection