@extends('layouts.admin')
@section('contenido')
<div class="card mb-3">
    <div class="card-header">
          <h4>Ingresos</h4>          
     </div>
  	@include('ingresos.search')

        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Conceptos</th>                 
                  <th>Precio de costo por unidad</th>
                  <th>Cantidad Anual</th>
                  <th>Precio Unitario Promedios</th>
                  <th>Ingresos anuales</th>
                  <th>Acción</th>
                </tr>
              </thead>

              <tfoot>
                    <tr>
                      <th></th>
                      <th></th>
                      <th></th>
                      <th></th>
                      <th>TOTAL</th>
                      @if($total->total < 1)
                        <th>0</th>
                      @else
                          <th>{{$total->total}}</th>                  
                      @endif
                      <th></th>
                    </tr>
                </tfoot>

              <tbody>
              	@foreach($ingresos as $ing)
					        <tr>
                        <td>{{$ing->id_ingreso}}</td>
                        <td>{{$ing->concepto}}</td>
                        <td>{{$ing->precio_costo}}</td>                      
                        <td>{{$ing->cantidad_anual}}</td>                      
                        <td>{{$ing->precio_unitario}}</td>                      
    	                  <td>{{$ing->ingreso_anual}}</td>                      
                        <td>
                            <a href="{{URL::action('IngresosController@edit',$ing->id_ingreso)}}"><button class="btn btn-info">Agregar Precio Unitario</button></a>                                     					                           
	                      </td>
                	</tr>       
              	@endforeach

              </tbody>
            </table>
          </div>
          {{$ingresos->render()}}
        </div>
        <div class="card-footer small text-muted">Desarrollado por Jorge Diaz</div>
      </div>
@endsection