@extends('layouts.admin')
@section('contenido')
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm6 col-xs-12">
			<h3>Agregar Precio Unitario de Ingresos</h3>

			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li> {{$error}} </li>
						@endforeach
					</ul>
				</div>
			@endif

			{!!Form::model($ingresos, ['method' => 'PATCH', 'route' => ['ingresos.update', $ingresos->id_ingreso]])!!}			
				{{Form::token()}}				

				<div class="form-group">
					<label for="nomina">Concepto de {{$ingresos->concepto}}</label>
				</div>

				<div class="form-group">
				 	<label for="precio_unitario">Precio unitario</label>					
			      	<input id="msg" type="text" class="form-control" name="precio_unitario" value="{{$ingresos->precio_unitario}}" placeholder="Precio unitario">
			    </div>
			    
				<div class="form-group">
					<button class="btn btn-primary" type="submit">Guardar</button>

					<button class="btn btn-danger" type="reset">Cancelar</button>
				</div>	
			{!!Form::close()!!}
		</div>
	</div>
@endsection