@extends('layouts.admin')
@section('contenido')
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm6 col-xs-12">
			<h3>Editar gasto de oficina</h3>

			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li> {{$error}} </li>
						@endforeach
					</ul>
				</div>
			@endif

			{!!Form::model($gastos_oficina,['method' => 'PATCH', 'route' => ['oficina.update', $gastos_oficina->id_gasto_oficina]])!!}			
				{{Form::token()}}
				<div class="form-group">
					<label>Proyectos</label>
					<select name="id_proyecto" class="form-control" id="">
						@foreach($proyecto as $p)
							@if($p->id_proyecto == $gastos_oficina->id_proyecto)
								<option value="{{$p->id_proyecto}}" selected>{{$p->nombre_proyecto}}</option>
							@else
								<option value="{{$p->id_proyecto}}">{{$p->nombre_proyecto}}</option>
							@endif
						@endforeach
					</select>
				</div>
				
				<div class="form-group">
					<label for="concepto">Concepto</label>
					<input type="text" name="concepto" class="form-control" value="{{$gastos_oficina->concepto}}" placeholder="Concepto">
				</div>


				<div class="form-group">
				  <label for="cantidad_diaria">Cantidad diaria</label>					
			      <input id="msg" type="text" class="form-control" name="cantidad_diaria" value="{{$gastos_oficina->cantidad_diaria}}" placeholder="Cantidad diaria">
			    </div>
				
				<div class="form-group">
				  <label for="unidad">Unidad</label>					
			      <input id="msg" type="text" class="form-control" name="unidad" value="{{$gastos_oficina->unidad}}" placeholder="Unidad">
			    </div>

			   	<div class="form-group">
				  <label for="costo_unitario">Costo unitario</label>					
			      <input id="msg" type="text" class="form-control" name="costo_unitario" value="{{$gastos_oficina->costo_unitario}}" placeholder="Costo unitario">
			    </div>
			    
				<div class="form-group">
					<button class="btn btn-primary" type="submit">Guardar</button>

					<button class="btn btn-danger" type="reset">Cancelar</button>
				</div>	
			{!!Form::close()!!}
		</div>
	</div>
@endsection