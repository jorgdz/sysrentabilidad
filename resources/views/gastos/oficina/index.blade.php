@extends('layouts.admin')
@section('contenido')
<div class="card mb-3">
     <div class="card-header">
          <i class="fa fa-table"></i>          
          <a href="oficina/create" class="btn btn-primary" type ="submit" name="btn_nuevo">Nuevo gasto de oficina</a>
     </div>
  	@include('gastos.oficina.search')

        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Concepto</th>
                  <th>Cantidad diaria</th>
                  <th>Unidad</th>
                  <th>Costo unitario</th>
                  <th>Costo total diario</th>
                  <th>Costo Anual</th>
                  <th>Acción</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th>Total</th>
                    @if($total_gastos_oficina->total < 1)
                      <th>0</th>
                    @else
                      <th><h4 id="tt">{{ $total_gastos_oficina->total }}</h4></th>  
                    @endif  
                    <th></th>                   
                </tr>                
              </tfoot>

              <tbody>

              	@foreach($gastos_oficina as $go)
					        <tr>
    	              <td>{{$go->id_gasto_oficina}}</td>
    	              <td>{{$go->concepto}}</td>
    	              <td>{{$go->cantidad_diaria}}</td>
                    <td>{{$go->unidad}}</td>
                    <td>{{$go->costo_unitario}}</td>
                    <td>{{$go->costo_total_diario}}</td>
                    <td>{{$go->costo_anual}}</td>
                    <td>
              				<a href="{{URL::action('GastosOficinaController@edit',$go->id_gasto_oficina)}}"><button class="btn btn-info">Editar</button></a>                            						
	                  </td>
                	</tr> 
              	@endforeach

              </tbody>
            </table>
          </div>
          {{$gastos_oficina->render()}}
        </div>
        <div class="card-footer small text-muted">Desarrollado por Jorge Diaz</div>
      </div>
@endsection