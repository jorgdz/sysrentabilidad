@extends('layouts.admin')
@section('contenido')
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm6 col-xs-12">
			<h3>Editar gasto general</h3>

			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li> {{$error}} </li>
						@endforeach
					</ul>
				</div>
			@endif

			{!!Form::model($gastos_generales,['method' => 'PATCH', 'route' => ['generales.update', $gastos_generales->id_gasto_general]])!!}			
				{{Form::token()}}
				<div class="form-group">
					<label>Proyectos</label>
					<select name="id_proyecto" class="form-control" id="">
						@foreach($proyecto as $p)
							@if($p->id_proyecto == $gastos_generales->id_proyecto)
								<option value="{{$p->id_proyecto}}" selected>{{$p->nombre_proyecto}}</option>
							@else
								<option value="{{$p->id_proyecto}}">{{$p->nombre_proyecto}}</option>
							@endif
						@endforeach
					</select>
				</div>
				
				<div class="form-group">
					<label for="concepto">Concepto</label>
					<input type="text" name="concepto" class="form-control" value="{{$gastos_generales->concepto}}" placeholder="Concepto">
				</div>


				<div class="form-group">
				  <label for="valor_mensual">Valor mensual</label>					
			      <input id="msg" type="text" class="form-control" name="valor_mensual" value="{{$gastos_generales->valor_mensual}}" placeholder="Valor mensual">
			    </div>
			    
				<div class="form-group">
					<button class="btn btn-primary" type="submit">Guardar</button>

					<button class="btn btn-danger" type="reset">Cancelar</button>
				</div>	
			{!!Form::close()!!}
		</div>
	</div>
@endsection