@extends('layouts.admin')
@section('contenido')
<div class="card mb-3">
     <div class="card-header">
          <i class="fa fa-table"></i>          
          <a href="generales/create" class="btn btn-primary" type ="submit" name="btn_nuevo">Nuevo gasto general</a>
     </div>
  	@include('gastos.generales.search')

        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Concepto</th>
                  <th>Valor mensual</th>
                  <th>Costo Anual</th>
                  <th>Acción</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                    <th></th>
                    <th></th>
                    <th>Total</th>
                    @if($total_gastos_generales->total < 1)
                      <th>0</th>
                    @else
                      <th><h4 id="tt">{{ $total_gastos_generales->total }}</h4></th>  
                    @endif  
                    <th></th>                   
                </tr>                
              </tfoot>

              <tbody>

              	@foreach($gastos_generales as $gg)
					        <tr>
    	              <td>{{$gg->id_gasto_general}}</td>
    	              <td>{{$gg->concepto}}</td>
    	              <td>{{$gg->valor_mensual}}</td>
                    <td>{{$gg->costo_anual}}</td>
                    <td>
              				<a href="{{URL::action('GastosGeneralesController@edit',$gg->id_gasto_general)}}"><button class="btn btn-info">Editar</button></a>                            						
	                  </td>
                	</tr> 
              	@endforeach

              </tbody>
            </table>
          </div>
          {{$gastos_generales->render()}}
        </div>
        <div class="card-footer small text-muted">Desarrollado por Jorge Diaz</div>
      </div>
@endsection