@extends('layouts.admin')
@section('contenido')
<div class="card mb-3">
     <div class="card-header">
          <i class="fa fa-table"></i>          
          <a href="ventas/create" class="btn btn-primary" type ="submit" name="btn_nuevo">Nuevo gasto de ventas</a>
     </div>
  	@include('gastos.ventas.search')

        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Concepto</th>
                  <th>Costo</th>
                  <th>Acción</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th></th>
                  <th>Imprevistos 5%</th>
                  @if($total_gastos_ventas->total < 1 )
                    <th>0</th>
                  @else
                    <th><h4>{{$total_gastos_ventas->total * 0.05}}</h4></th>
                  @endif
                </tr>
                <tr>
                    <th></th>
                    <th>Total</th>
                    @if($total_gastos_ventas->total < 1)
                      <th>0</th>
                    @else
                      <th><h4 id="tt">{{ round($total_gastos_ventas->total + ($total_gastos_ventas->total * 0.05), 3)}}</h4></th>  
                    @endif  
                    <th></th>                   
                </tr>                
              </tfoot>

              <tbody>

              	@foreach($gastos_ventas as $gv)
					        <tr>
    	              <td>{{$gv->id_gastos_venta}}</td>
    	              <td>{{$gv->concepto}}</td>
    	              <td>{{$gv->costo}}</td>
                    <td>
              				<a href="{{URL::action('GastosVentasController@edit',$gv->id_gastos_venta)}}"><button class="btn btn-info">Editar</button></a>                            						
	                  </td>
                	</tr> 
              	@endforeach

              </tbody>
            </table>
          </div>
          {{$gastos_ventas->render()}}
        </div>
        <div class="card-footer small text-muted">Desarrollado por Jorge Diaz</div>
      </div>
@endsection