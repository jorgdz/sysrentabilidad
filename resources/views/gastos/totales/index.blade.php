@extends('layouts.admin')
@section('contenido')
<div class="card mb-3">
    <div class="card-header">
          <h4>Total de Gasto de administración</h4>          
     </div>
  	@include('gastos.totales.search')

        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Conceptos</th>                 
                  <th>Costos</th>
                  <th>Acción</th>
                </tr>
              </thead>

              <tfoot>
                    <tr>
                      <th></th>
                      <th>Imprevistos 5%</th>
                      @if($total_gasto_admin_costo->total < 1)
                        <th>0</th>
                      @else
                        <th>{{$total_gasto_admin_costo->total * 0.05}}</th>                     
                      @endif
                      <th></th>                       
                    </tr>
                    <tr>
                      <th></th>
                      <th>TOTAL</th>
                      @if($total_gasto_admin_costo->total < 1)
                        <th>0</th>
                      @else
                          <th>{{$total_gasto_admin_costo->total + ($total_gasto_admin_costo->total * 0.05)}}</th>                  
                      @endif
                      <th></th>
                    </tr>
                </tfoot>

              <tbody>
              	@foreach($total_gasto_admin as $tga)
					        <tr>
                        <td>{{$tga->id_gastos_administracion}}</td>
                        <td>{{$tga->conceptos}}</td>
    	                  <td>{{$tga->costos}}</td>                      
                        <td>
                          @if($tga->costos == 0)
              						  <a href="{{URL::action('TotalGastosAdministracionController@edit',$tga->id_gastos_administracion)}}"><button class="btn btn-info">Agregar Valores</button></a>                                     						                           
                          @endif
	                      </td>
                	</tr>       
              	@endforeach

              </tbody>
            </table>
          </div>
          {{$total_gasto_admin->render()}}
        </div>
        <div class="card-footer small text-muted">Desarrollado por Jorge Diaz</div>
      </div>
@endsection