@extends('layouts.admin')
@section('contenido')
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm6 col-xs-12">
			<h3>Agregar nuevo valor</h3>

			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li> {{$error}} </li>
						@endforeach
					</ul>
				</div>
			@endif

			{!!Form::model($total_gasto_admin, ['method' => 'PATCH', 'route' => ['totales.update', $total_gasto_admin->id_gastos_administracion]])!!}			
				{{Form::token()}}				

				<div class="form-group">
					<label for="conceptos">Concepto de {{$total_gasto_admin->conceptos}}</label>
				</div>

				<div class="form-group">
					<label for="costos">Costos</label>					
			     	 <input id="msg" type="text" class="form-control" name="costos"  value="{{$total_gasto_admin->costos}}" placeholder="Costos">
			    </div>

				<div class="form-group">
					<button class="btn btn-primary" type="submit">Guardar</button>

					<button class="btn btn-danger" type="reset">Cancelar</button>
				</div>	
			{!!Form::close()!!}
		</div>
	</div>
@endsection