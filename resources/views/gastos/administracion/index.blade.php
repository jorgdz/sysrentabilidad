@extends('layouts.admin')
@section('contenido')
<div class="card mb-3">
    <div class="card-header">
          <h4>Gastos de administración</h4>          
     </div>
  	@include('gastos.administracion.search')

        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Conceptos</th>
                  <th>Valor Anual</th>
                  <th>Prestaciones Sociales</th>
                  <th>Sueldo + prestaciones</th>
                </tr>
              </thead>

              <tfoot>
                    <tr>
                      <th></th>
                      <th></th>
                      <th></th>
                      <th>Total</th>
                      @if($total->total < 1)
                        <th>0</th>
                      @else
                        <th>{{$total->total}}</th>
                      @endif
                    </tr>
                </tfoot>

              <tbody>
              	@foreach($gastos_admin as $ga)
					        <tr>
                        <td>{{$ga->id_gastos_administracion}}</td>
                        <td>{{$ga->concepto}}</td>
                        <td>{{$ga->valor_anual}}</td>                       
                        <td>{{$ga->presta_sociales}}</td>                       
    	                  <td>{{$ga->sueldo_prestaciones}}</td>                       
                	</tr>       
              	@endforeach

              </tbody>
            </table>
          </div>
          {{$gastos_admin->render()}}
        </div>
        <div class="card-footer small text-muted">Desarrollado por Jorge Diaz</div>
      </div>
@endsection