<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TablaAmortizacion extends Model
{
    protected $table = 'tabla_amortizacion';
    protected $primaryKey = 'id_tabla_amortizacion';
    public $timestamps = false;

    protected $fillable = [
		'id_proyecto',
  		'saldo_deuda',
		'cuotas',
		'interes',
		'amortizacion'
    ];
}
