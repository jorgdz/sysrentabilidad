<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FlujoCaja extends Model
{
    protected $table = 'flujo_caja';
    protected $primaryKey = 'id_flujo_caja';
    public $timestamps = false;

    protected $fillable = [
		'id_proyecto',
		'ingresos',
		'inicial',
		'anio1',
		'anio2',
		'anio3',
		'anio4',
		'anio5'
    ];
}
