<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GastosGenerales extends Model
{
    protected $table = 'gastos_generales';
    protected $primaryKey = 'id_gasto_general';
    public $timestamps = false;

    protected $fillable = [
		'id_proyecto',
  		'concepto',
  		'valor_mensual',
  		'costo_anual'
    ];
}
