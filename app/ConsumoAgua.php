<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConsumoAgua extends Model
{
    protected $table = 'consumo_agua';
    protected $primaryKey = 'id_consumo_agua';
    public $timestamps = false;

    protected $fillable = [
		'id_proyecto',
		'concepto',
		'cantidad',
		'consumo_anual',
		'costo_unitario',
		'costo_anual'
    ];
}
