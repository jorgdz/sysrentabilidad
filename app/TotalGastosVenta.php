<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TotalGastosVenta extends Model
{
    protected $table = 'total_gastos_venta';
    protected $primaryKey = 'id_gastos_venta';
    public $timestamps = false;

    protected $fillable = [
		'id_proyecto',
  		'concepto',
  		'costo'
    ];
}
