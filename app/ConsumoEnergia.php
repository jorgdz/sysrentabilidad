<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConsumoEnergia extends Model
{
    protected $table = 'consumo_energia';
    protected $primaryKey = 'id_consumo_energia';
    public $timestamps = false;

    protected $fillable = [
		'id_proyecto',
  		'descripcion',
		'consumo_mensual',
		'consumo_anual' 
    ];
}
