<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MateriaPrima extends Model
{
    protected $table = 'materia_prima';
    protected $primaryKey = 'id_materia_prima';
    public $timestamps = false;

    protected $fillable = [
		  'id_proyecto',
  		'descripcion',
  		'costo_unitario',
  		'demanda_anual',
  		'costo_total_anual'
    ];
}
