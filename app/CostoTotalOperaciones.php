<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CostoTotalOperaciones extends Model
{
    protected $table = 'costo_total_operaciones';
    protected $primaryKey = 'id_costo_total_operacion';
    public $timestamps = false;

    protected $fillable = [
		'id_proyecto',
  		'concepto',
  		'costo',
  		'porcentaje'
    ];
}
