<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingresos extends Model
{
    protected $table = 'ingresos';
    protected $primaryKey = 'id_ingreso';
    public $timestamps = false;

    protected $fillable = [
		  'id_proyecto',
  		'concepto',
  		'precio_costo',
  		'cantidad_anual',
  		'precio_unitario',
  		'ingreso_anual'
    ];
}
