<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CapitalTrabajo extends Model
{
    protected $table = 'capital_trabajo';
    protected $primaryKey = 'id_capital_trabajo';
    public $timestamps = false;

    protected $fillable = [
		'id_proyecto',
  		'concepto',
  		'valor'
    ];
}
