<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CostoProduccionUnitario extends Model
{
    protected $table = 'costo_produccion_unitario';
    protected $primaryKey = 'id_costo_produccion_unitario';
    public $timestamps = false;

    protected $fillable = [
		'id_proyecto',
  		'conceptos',
  		'valor_unitario'
    ];
}
