<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GastosAdministracion extends Model
{
    protected $table = 'gastos_administracion';
    protected $primaryKey = 'id_gastos_administracion';
    public $timestamps = false;

    protected $fillable = [
		  'id_proyecto',
  		'concepto',
  		'valor_anual',
  		'presta_sociales',
  		'sueldo_prestaciones'
    ];
}
