<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prestaciones extends Model
{
    protected $table = 'prestaciones';
    protected $primaryKey = 'id_prestaciones';
    public $timestamps = false;

    protected $fillable = [
		  'id_proyecto',
  		'nomina',
  		'numero',
  		'sueldo_mensual',
  		'horas_suplementarias',
  		'sueldo_anual',
  		'treceavo_sueldo',
  		'catorceavo_sueldo',
  		'fondo_reserva',
  		'vacaciones',
  		'aporte_iess',
  		'total_prestaciones_primer_aniol'
    ];
}
