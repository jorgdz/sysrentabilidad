<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PresupuestoCostoProduccion extends Model
{
    protected $table = 'presupuesto_costo_produccion';
    protected $primaryKey = 'id_presupuesto_costo_produccion';
    public $timestamps = false;

    protected $fillable = [
		'id_proyecto',
  		'concepto',
  		'valor_anual'
    ];
}
