<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TotalGastosAdministracion extends Model
{
    protected $table = 'total_gastos_administracion';
    protected $primaryKey = 'id_gastos_administracion';
    public $timestamps = false;

    protected $fillable = [
		'id_proyecto',
  		'conceptos',
  		'costos'
    ];
}
