<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoResultado extends Model
{
    protected $table = 'estado_resultado';
    protected $primaryKey = 'id_estado_resultado';
    public $timestamps = false;

    protected $fillable = [
		  'id_proyecto',
  		'ingresos' ,
  		'anio1',
		  'anio2',
		  'anio3',
		  'anio4',
		  'anio5'
    ];
}
