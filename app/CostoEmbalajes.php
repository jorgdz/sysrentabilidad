<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CostoEmbalajes extends Model
{
    protected $table = 'costo_embalajes';
    protected $primaryKey = 'id_costo_embalaje';
    public $timestamps = false;

    protected $fillable = [
		'id_proyecto',
	  	'descripcion',
	  	'costo_unitario',
	  	'cantidad',
	  	'costo_anuales'
    ];
}
