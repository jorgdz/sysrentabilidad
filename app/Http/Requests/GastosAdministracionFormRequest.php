<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GastosAdministracionFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_proyecto' => 'required',
            'concepto' => 'required|max:90',
            'valor_anual' => 'numeric',
            'presta_sociales' => 'numeric',
            'sueldo_prestaciones' => 'numeric'
        ];
    }
}
