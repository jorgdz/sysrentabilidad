<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PrestacionesFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_proyecto' => 'numeric',
            'nomina' => 'max:80',
            'numero' => 'numeric',
            'sueldo_mensual' => 'numeric',
            'horas_suplementarias' => 'numeric',
            'sueldo_anual' => 'numeric',
            'treceavo_sueldo' => 'numeric',
            'catorceavo_sueldo' => 'numeric',
            'fondo_reserva' => 'numeric',
            'vacaciones' => 'numeric',
            'aporte_iess' => 'numeric',
            'total_prestaciones_primer_aniol' => 'numeric'
        ];
    }
}
