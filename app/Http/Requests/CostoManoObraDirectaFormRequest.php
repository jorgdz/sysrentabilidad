<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CostoManoObraDirectaFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_proyecto' => 'numeric',
            'personal' => 'max:90',
            'cantidad' => 'numeric',
            'remuneracion' => 'numeric',
            'monto_total' => 'numeric'
        ];
    }
}
