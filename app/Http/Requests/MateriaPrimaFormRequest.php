<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MateriaPrimaFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_proyecto' => 'required',
            'descripcion' => 'required|max:90',
            'costo_unitario' => 'numeric',
            'demanda_anual' => 'numeric',
            'costo_total_anual' => 'numeric'
        ];
    }
}
