<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GastosOficinaFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_proyecto' => 'required',
            'concepto' => 'required|max:90',
            'cantidad_diaria' => 'numeric',
            'unidad' => 'max:90',
            'costo_unitario' => 'numeric',
            'costo_total_diario' => 'numeric',
            'costo_anual' => 'numeric'
        ];
    }
}
