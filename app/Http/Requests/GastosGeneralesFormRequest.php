<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GastosGeneralesFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_proyecto' => 'required',
            'concepto' => 'required|max:90',
            'valor_mensual' => 'numeric',
            'costo_anual' => 'numeric'
        ];
    }
}
