<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IngresosFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_proyecto' => 'numeric',
            'concepto' => 'max:99',
            'precio_costo' => 'numeric',
            'cantidad_anual' => 'numeric',
            'precio_unitario' => 'numeric',
            'ingreso_anual' => 'numeric'
        ];
    }
}
