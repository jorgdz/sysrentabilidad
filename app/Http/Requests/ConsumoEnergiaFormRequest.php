<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ConsumoEnergiaFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_proyecto' => 'numeric',
            'descripcion' => 'max:90',
            'consumo_mensual' => 'numeric',
            'consumo_anual' => 'numeric'
        ];
    }
}
