<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\CostoEmbalajes;

use Illuminate\Support\Facades\Redirect;

use App\Http\Requests\CostoEmbalajesFormRequest;

use DB;

use Carbon\Carbon;

use Response;

use Illuminate\Support\Collection;

class CostoEmbalajeController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    } 

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request) {
            $proyecto = DB::table('proyecto as p')->orderBy('p.id_proyecto','desc')->get();

            $lista_proyecto = $request->get('id_proyecto');

            $costo_embalaje = DB::table('costo_embalajes as ce')
            ->join('proyecto as p', 'ce.id_proyecto', '=', 'p.id_proyecto')
            ->select('ce.id_costo_embalaje', 'ce.descripcion', 'ce.costo_unitario', 'ce.cantidad', 'ce.costo_anuales')
            ->where('ce.id_proyecto','=',$lista_proyecto)
            ->orderBy('ce.id_costo_embalaje','asc')
            ->groupBy('ce.id_costo_embalaje', 'ce.descripcion', 'ce.costo_unitario', 'ce.cantidad', 'ce.costo_anuales')
            ->paginate(7);

            $totales_materia_prima = DB::table('materia_prima as mp') 
            ->select(DB::raw('sum(mp.demanda_anual) as total1'), DB::raw('sum(mp.costo_total_anual) as total2'))
            ->where('mp.id_proyecto','=',$lista_proyecto)
            ->first();

            $totales_costos_embalajes = DB::table('costo_embalajes as ce') 
            ->select(DB::raw('sum(ce.costo_anuales) as total1'))
            ->where('ce.id_proyecto','=',$lista_proyecto)
            ->first();

            return view('costos.embalaje.index', ['costoembalaje' => $costo_embalaje, 'proyecto' => $proyecto, 'total' => $totales_costos_embalajes, 'totalmp' => $totales_materia_prima]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $proyecto = DB::table('proyecto as p')->orderBy('p.id_proyecto','desc')->get();
        return view('costos.embalaje.create', ["proyecto" => $proyecto]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CostoEmbalajesFormRequest $request)
    {
        $ce = new CostoEmbalajes;
        $ce->id_proyecto = $request->get('id_proyecto');
        $ce->descripcion = $request->get('descripcion');
        $ce->costo_unitario = $request->get('costo_unitario');
        $ce->cantidad = $request->get('cantidad');
        $ce->costo_anuales = $request->get('costo_unitario') * $request->get('cantidad');
        $ce->save();

        return Redirect::to('costos/embalaje');      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $proyecto = DB::table('proyecto as p')->orderBy('p.id_proyecto','desc')->get();
        return view("costos.embalaje.edit",['costoembalaje' => CostoEmbalajes::findOrFail($id), 'proyecto' => $proyecto]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CostoEmbalajesFormRequest $request, $id)
    {
        $ce = CostoEmbalajes::findOrFail($id);
        $ce->id_proyecto = $request->get('id_proyecto');
        $ce->descripcion = $request->get('descripcion');
        $ce->costo_unitario = $request->get('costo_unitario');
        $ce->cantidad = $request->get('cantidad');
        $ce->costo_anuales = $request->get('costo_unitario') * $request->get('cantidad'); 
        $ce->update();
        return Redirect::to('costos/embalaje');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
