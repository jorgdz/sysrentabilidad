<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Proyecto;

use App\Prestaciones;

use App\ConsumoAgua;

use App\Ingresos;

use App\FlujoCaja;

use App\CapitalTrabajo;

use App\ConsumoEnergia;

use App\TotalGastosAdministracion;

use App\GastosAdministracion;

use App\PresupuestoCostoProduccion;

use App\CostoProduccionUnitario;

use Illuminate\Support\Facades\Redirect;

use App\Http\Requests\ProyectoFormRequest;

use DB;

class ProyectoController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request) {
            $query = trim($request->get('search'));
            $proyecto = DB::table('proyecto')->where('nombre_proyecto','LIKE','%'.$query.'%')
            ->orderBy('id_proyecto','desc')->paginate(7);
            return view('proyectos.proyecto.index', ["proyecto" => $proyecto, "search" => $query]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("proyectos.proyecto.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProyectoFormRequest $request)
    {
        $proyecto = new Proyecto;
        $proyecto->nombre_proyecto = $request->get('nombre_proyecto');
        $proyecto->descripcion = $request->get('descripcion');
        $proyecto->save();

        $cpu1 = new CostoProduccionUnitario;
        $cpu1->id_proyecto = $proyecto->id_proyecto;
        $cpu1->conceptos = 'Materia prima';
        $cpu1->valor_unitario = 0;
        $cpu1->save();

        $cpu2 = new CostoProduccionUnitario;      
        $cpu2->id_proyecto = $proyecto->id_proyecto;
        $cpu2->conceptos = 'Embalaje';
        $cpu2->valor_unitario = 0;
        $cpu2->save();
        
        $cpu3 = new CostoProduccionUnitario;
        $cpu3->id_proyecto = $proyecto->id_proyecto;
        $cpu3->conceptos = 'Otros materiales';
        $cpu3->valor_unitario = 0;
        $cpu3->save();
        
        $cpu4 = new CostoProduccionUnitario;
        $cpu4->id_proyecto = $proyecto->id_proyecto;
        $cpu4->conceptos = 'Energía eléctrica';
        $cpu4->valor_unitario = 0;
        $cpu4->save();
        
        $cpu5 = new CostoProduccionUnitario;
        $cpu5->id_proyecto = $proyecto->id_proyecto;
        $cpu5->conceptos = 'Agua';
        $cpu5->valor_unitario = 0;
        $cpu5->save();
        
        $cpu6 = new CostoProduccionUnitario;
        $cpu6->id_proyecto = $proyecto->id_proyecto;
        $cpu6->conceptos = 'Mantenimiento';
        $cpu6->valor_unitario = 0;
        $cpu6->save();
        
        $cpu7 = new CostoProduccionUnitario;
        $cpu7->id_proyecto = $proyecto->id_proyecto;
        $cpu7->conceptos = 'Mano de obra directa';
        $cpu7->valor_unitario = 0;
        $cpu7->save();

        $cpu8 = new CostoProduccionUnitario;
        $cpu8->id_proyecto = $proyecto->id_proyecto;
        $cpu8->conceptos = 'Imprevistos';
        $cpu8->valor_unitario = 0;
        $cpu8->save();

        /****************Prestaciones*******************/
        
        $prest1 = new Prestaciones;
        $prest1->id_proyecto = $proyecto->id_proyecto;
        $prest1->nomina = 'Gerente';
        $prest1->numero = 0;
        $prest1->sueldo_mensual = 0;
        $prest1->horas_suplementarias = 0;
        $prest1->sueldo_anual = 0;
        $prest1->treceavo_sueldo = 0;
        $prest1->catorceavo_sueldo = 0;
        $prest1->fondo_reserva = 0;
        $prest1->vacaciones = 0;
        $prest1->aporte_iess = 0;
        $prest1->total_prestaciones_primer_aniol = 0; 
        $prest1->save();

        $prest2 = new Prestaciones;
        $prest2->id_proyecto = $proyecto->id_proyecto;
        $prest2->nomina = 'Contadora Secretaria y recepcionista';
        $prest2->numero = 0;
        $prest2->sueldo_mensual = 0;
        $prest2->horas_suplementarias = 0;
        $prest2->sueldo_anual = 0;
        $prest2->treceavo_sueldo = 0;
        $prest2->catorceavo_sueldo = 0;
        $prest2->fondo_reserva = 0;
        $prest2->vacaciones = 0;
        $prest2->aporte_iess = 0;
        $prest2->total_prestaciones_primer_aniol = 0;
        $prest2->save();
        
        $prest3 = new Prestaciones;
        $prest3->id_proyecto = $proyecto->id_proyecto;
        $prest3->nomina = 'Empleados - diseñadores';
        $prest3->numero = 0;
        $prest3->sueldo_mensual = 0;
        $prest3->horas_suplementarias = 0;
        $prest3->sueldo_anual = 0;
        $prest3->treceavo_sueldo = 0;
        $prest3->catorceavo_sueldo = 0;
        $prest3->fondo_reserva = 0;        
        $prest3->vacaciones = 0;
        $prest3->aporte_iess = 0;
        $prest3->total_prestaciones_primer_aniol = 0;
        $prest3->save();
        
        $prest4 = new Prestaciones;
        $prest4->id_proyecto = $proyecto->id_proyecto;
        $prest4->nomina = 'Bodeguero y limpieza';
        $prest4->numero = 0;
        $prest4->sueldo_mensual = 0;
        $prest4->horas_suplementarias = 0;
        $prest4->sueldo_anual = 0;
        $prest4->treceavo_sueldo = 0;
        $prest4->catorceavo_sueldo = 0;
        $prest4->fondo_reserva = 0;
        $prest4->vacaciones = 0;
        $prest4->aporte_iess = 0;
        $prest4->total_prestaciones_primer_aniol = 0;
        $prest4->save();
        
        $prest5 = new Prestaciones;
        $prest5->id_proyecto = $proyecto->id_proyecto;
        $prest5->nomina = 'Operarios';
        $prest5->numero = 0;
        $prest5->sueldo_mensual = 0;   
        $prest5->horas_suplementarias = 0;
        $prest5->sueldo_anual = 0;
        $prest5->treceavo_sueldo = 0;
        $prest5->catorceavo_sueldo = 0;
        $prest5->fondo_reserva = 0;
        $prest5->vacaciones = 0;
        $prest5->aporte_iess = 0;
        $prest5->total_prestaciones_primer_aniol = 0;
        $prest5->save();    

        /****************Consumos****************/ 

        $ca = new ConsumoAgua;
        $ca->id_proyecto = $proyecto->id_proyecto;
        $ca->concepto = 'Agua';
        $ca->cantidad = 0;
        $ca->consumo_anual = 0;
        $ca->costo_unitario = 0;
        $ca->costo_anual = 0;
        $ca->save();

        $ce = new ConsumoEnergia;
        $ce->id_proyecto = $proyecto->id_proyecto;
        $ce->descripcion = 'Energía eléctrica';
        $ce->consumo_mensual = 0;
        $ce->consumo_anual = 0;
        $ce->save();

        /****************PresupuestoCostoProduccion********************/

        $pcp1 = new PresupuestoCostoProduccion;
        $pcp1->id_proyecto = $proyecto->id_proyecto;
        $pcp1->concepto = 'Materia prima';
        $pcp1->valor_anual = 0;
        $pcp1->save();

        $pcp2 = new PresupuestoCostoProduccion;
        $pcp2->id_proyecto = $proyecto->id_proyecto;
        $pcp2->concepto = 'Embalaje';
        $pcp2->valor_anual = 0;
        $pcp2->save();

        $pcp3 = new PresupuestoCostoProduccion;
        $pcp3->id_proyecto = $proyecto->id_proyecto;
        $pcp3->concepto = 'Otros materiales';
        $pcp3->valor_anual = 0;
        $pcp3->save();

        $pcp4 = new PresupuestoCostoProduccion;
        $pcp4->id_proyecto = $proyecto->id_proyecto;
        $pcp4->concepto = 'Energía eléctrica';
        $pcp4->valor_anual = 0;
        $pcp4->save();

        $pcp5 = new PresupuestoCostoProduccion;
        $pcp5->id_proyecto = $proyecto->id_proyecto;
        $pcp5->concepto = 'Agua';
        $pcp5->valor_anual = 0;
        $pcp5->save();

        $pcp6 = new PresupuestoCostoProduccion;
        $pcp6->id_proyecto = $proyecto->id_proyecto;
        $pcp6->concepto = 'Mantenimiento';
        $pcp6->valor_anual = 0;
        $pcp6->save();

        $pcp7 = new PresupuestoCostoProduccion;
        $pcp7->id_proyecto = $proyecto->id_proyecto;
        $pcp7->concepto = 'Mano de Obra directa';
        $pcp7->valor_anual = 0;
        $pcp7->save();


        
 /*******************Gastos de administración*********************/
        $ga1 = new GastosAdministracion;
        $ga1->id_proyecto = $proyecto->id_proyecto;
        $ga1->concepto = 'Gerente';
        $ga1->valor_anual = 0;
        $ga1->presta_sociales = 0;
        $ga1->sueldo_prestaciones = 0;
        $ga1->save();

        $ga2 = new GastosAdministracion;
        $ga2->id_proyecto = $proyecto->id_proyecto;
        $ga2->concepto = 'Contadora Secretaria y recepcionista';
        $ga2->valor_anual = 0;
        $ga2->presta_sociales = 0;
        $ga2->sueldo_prestaciones = 0;
        $ga2->save();

        /*****************TotalGastosAdministracion*******************/

        $tga1 = new TotalGastosAdministracion;
        $tga1->id_proyecto = $proyecto->id_proyecto;
        $tga1->conceptos = 'Sueldo de Personal';
        $tga1->costos = 0;
        $tga1->save();

        $tga2 = new TotalGastosAdministracion;
        $tga2->id_proyecto = $proyecto->id_proyecto;
        $tga2->conceptos = 'Gasto de oficina';
        $tga2->costos = 0;
        $tga2->save();

        $tga3 = new TotalGastosAdministracion;
        $tga3->id_proyecto = $proyecto->id_proyecto;
        $tga3->conceptos = 'Arriendo del local';
        $tga3->costos = 0;
        $tga3->save();
        
        $tga4 = new TotalGastosAdministracion;
        $tga4->id_proyecto = $proyecto->id_proyecto;
        $tga4->conceptos = 'Comida de empleados';
        $tga4->costos = 0;
        $tga4->save();

        /*************************CapitalTrabajo**************************/
        $ct1 = new CapitalTrabajo;
        $ct1->id_proyecto = $proyecto->id_proyecto;
        $ct1->concepto = 'Materia prima (1 mes)';
        $ct1->valor = 0;
        $ct1->save();

        $ct2 = new CapitalTrabajo;
        $ct2->id_proyecto = $proyecto->id_proyecto;
        $ct2->concepto = 'Materiales indirecto';
        $ct2->valor = 0;
        $ct2->save();

        $ct3 = new CapitalTrabajo;
        $ct3->id_proyecto = $proyecto->id_proyecto;
        $ct3->concepto = 'Mano de obra directa';
        $ct3->valor = 0;
        $ct3->save();
        
        $ct4 = new CapitalTrabajo;
        $ct4->id_proyecto = $proyecto->id_proyecto;
        $ct4->concepto = 'Gastos Administrativo';
        $ct4->valor = 0;
        $ct4->save();

        $ct5 = new CapitalTrabajo;
        $ct5->id_proyecto = $proyecto->id_proyecto;
        $ct5->concepto = 'Gastos de ventas';
        $ct5->valor = 0;
        $ct5->save();

        $ing = new Ingresos;
        $ing->id_proyecto = $proyecto->id_proyecto;
        $ing->concepto = 'Costo de producción unitario';
        $ing->precio_costo = 0;
        $ing->cantidad_anual = 0;
        $ing->precio_unitario = 1;
        $ing->ingreso_anual = 1;
        $ing->save();


        /*****************************FlujoCaja*******************************/
        $fc1 = new FlujoCaja;
        $fc1->id_proyecto = $proyecto->id_proyecto;
        $fc1->ingresos = 'Ventas';
        $fc1->inicial = 0;
        $fc1->anio1 = 0;
        $fc1->anio2 = 0;
        $fc1->anio3 = 0;
        $fc1->anio4 = 0;
        $fc1->anio5 = 0;
        $fc1->save();

        $fc2 = new FlujoCaja;
        $fc2->id_proyecto = $proyecto->id_proyecto;
        $fc2->ingresos = 'Préstamo';
        $fc2->inicial = 0;
        $fc2->anio1 = 0;
        $fc2->anio2 = 0;
        $fc2->anio3 = 0;
        $fc2->anio4 = 0;
        $fc2->anio5 = 0;
        $fc2->save();

        $fc3 = new FlujoCaja;
        $fc3->id_proyecto = $proyecto->id_proyecto;
        $fc3->ingresos = 'INGRESOS TOTALES';
        $fc3->inicial = 0;
        $fc3->anio1 = 0;
        $fc3->anio2 = 0;
        $fc3->anio3 = 0;
        $fc3->anio4 = 0;
        $fc3->anio5 = 0;
        $fc3->save();

        $fc5 = new FlujoCaja;
        $fc5->id_proyecto = $proyecto->id_proyecto;
        $fc5->ingresos = 'Costo de producción';
        $fc5->inicial = 0;
        $fc5->anio1 = 0;
        $fc5->anio2 = 0;
        $fc5->anio3 = 0;
        $fc5->anio4 = 0;
        $fc5->anio5 = 0;
        $fc5->save();  

        $fc6 = new FlujoCaja;
        $fc6->id_proyecto = $proyecto->id_proyecto;
        $fc6->ingresos = 'Gastos administrativos';
        $fc6->inicial = 0;
        $fc6->anio1 = 0;
        $fc6->anio2 = 0;
        $fc6->anio3 = 0;
        $fc6->anio4 = 0;
        $fc6->anio5 = 0;
        $fc6->save();  
         
        $fc7 = new FlujoCaja;
        $fc7->id_proyecto = $proyecto->id_proyecto;
        $fc7->ingresos = 'Gastos de oficina';
        $fc7->inicial = 0;
        $fc7->anio1 = 0;
        $fc7->anio2 = 0;
        $fc7->anio3 = 0;
        $fc7->anio4 = 0;
        $fc7->anio5 = 0;
        $fc7->save();
        
        $fc8 = new FlujoCaja;
        $fc8->id_proyecto = $proyecto->id_proyecto;
        $fc8->ingresos = 'Gastos de venta';
        $fc8->inicial = 0;
        $fc8->anio1 = 0;
        $fc8->anio2 = 0;
        $fc8->anio3 = 0;
        $fc8->anio4 = 0;
        $fc8->anio5 = 0;
        $fc8->save();
         
        $fc9 = new FlujoCaja;
        $fc9->id_proyecto = $proyecto->id_proyecto;
        $fc9->ingresos = 'Gastos generales';
        $fc9->inicial = 0;
        $fc9->anio1 = 0;
        $fc9->anio2 = 0;
        $fc9->anio3 = 0;
        $fc9->anio4 = 0;
        $fc9->anio5 = 0;
        $fc9->save();

        $fc10 = new FlujoCaja;
        $fc10->id_proyecto = $proyecto->id_proyecto;
        $fc10->ingresos = 'Gastos Financieros';
        $fc10->inicial = 0;
        $fc10->anio1 = 0;
        $fc10->anio2 = 0;
        $fc10->anio3 = 0;
        $fc10->anio4 = 0;
        $fc10->anio5 = 0;
        $fc10->save();

        $fc11 = new FlujoCaja;
        $fc11->id_proyecto = $proyecto->id_proyecto;
        $fc11->ingresos = 'Arriendo de Local';
        $fc11->inicial = 0;
        $fc11->anio1 = 0;
        $fc11->anio2 = 0;
        $fc11->anio3 = 0;
        $fc11->anio4 = 0;
        $fc11->anio5 = 0;
        $fc11->save();

        $fc12 = new FlujoCaja;
        $fc12->id_proyecto = $proyecto->id_proyecto;
        $fc12->ingresos = 'Comida de empleados';
        $fc12->inicial = 0;
        $fc12->anio1 = 0;
        $fc12->anio2 = 0;
        $fc12->anio3 = 0;
        $fc12->anio4 = 0;
        $fc12->anio5 = 0;
        $fc12->save();

        $fc13 = new FlujoCaja;
        $fc13->id_proyecto = $proyecto->id_proyecto;
        $fc13->ingresos = 'Depreciación';
        $fc13->inicial = 0;
        $fc13->anio1 = 0;
        $fc13->anio2 = 0;
        $fc13->anio3 = 0;
        $fc13->anio4 = 0;
        $fc13->anio5 = 0;
        $fc13->save();

        $fc14 = new FlujoCaja;
        $fc14->id_proyecto = $proyecto->id_proyecto;
        $fc14->ingresos = 'Amortización de intangibles';
        $fc14->inicial = 0;
        $fc14->anio1 = 0;
        $fc14->anio2 = 0;
        $fc14->anio3 = 0;
        $fc14->anio4 = 0;
        $fc14->anio5 = 0;
        $fc14->save();
        /*
        $fc15 = new FlujoCaja;
        $fc15->id_proyecto = $proyecto->id_proyecto;
        $fc15->ingresos = 'EGRESOS TOTALES';
        $fc15->inicial = 0;
        $fc15->anio1 = 0;
        $fc15->anio2 = 0;
        $fc15->anio3 = 0;
        $fc15->anio4 = 0;
        $fc15->anio5 = 0;
        $fc15->save();

        $fc16 = new FlujoCaja;
        $fc16->id_proyecto = $proyecto->id_proyecto;
        $fc16->ingresos = 'UTILIDAD ANTES DE IMPUESTOS';
        $fc16->inicial = 0;
        $fc16->anio1 = 0;
        $fc16->anio2 = 0;
        $fc16->anio3 = 0;
        $fc16->anio4 = 0;
        $fc16->anio5 = 0;
        $fc16->save();

        $fc17 = new FlujoCaja;
        $fc17->id_proyecto = $proyecto->id_proyecto;
        $fc17->ingresos = '15% De participación a trabajadores';
        $fc17->inicial = 0;
        $fc17->anio1 = 0;
        $fc17->anio2 = 0;
        $fc17->anio3 = 0;
        $fc17->anio4 = 0;
        $fc17->anio5 = 0;
        $fc17->save();

        $fc18 = new FlujoCaja;
        $fc18->id_proyecto = $proyecto->id_proyecto;
        $fc18->ingresos = 'UTILIDAD DESPUÉS DE PARTICIPACIÓN';
        $fc18->inicial = 0;
        $fc18->anio1 = 0;
        $fc18->anio2 = 0;
        $fc18->anio3 = 0;
        $fc18->anio4 = 0;
        $fc18->anio5 = 0;
        $fc18->save();

        $fc19 = new FlujoCaja;
        $fc19->id_proyecto = $proyecto->id_proyecto;
        $fc19->ingresos = '25% De impuesto a la renta';
        $fc19->inicial = 0;
        $fc19->anio1 = 0;
        $fc19->anio2 = 0;
        $fc19->anio3 = 0;
        $fc19->anio4 = 0;
        $fc19->anio5 = 0;
        $fc19->save();

        $fc20 = new FlujoCaja;
        $fc20->id_proyecto = $proyecto->id_proyecto;
        $fc20->ingresos = 'UTILIDAD DESPUÉS IMPUESTO A LA RENTA';
        $fc20->inicial = 0;
        $fc20->anio1 = 0;
        $fc20->anio2 = 0;
        $fc20->anio3 = 0;
        $fc20->anio4 = 0;
        $fc20->anio5 = 0;
        $fc20->save();

        $fc21 = new FlujoCaja;
        $fc21->id_proyecto = $proyecto->id_proyecto;
        $fc21->ingresos = 'Depreciación (+)';
        $fc21->inicial = 0;
        $fc21->anio1 = 0;
        $fc21->anio2 = 0;
        $fc21->anio3 = 0;
        $fc21->anio4 = 0;
        $fc21->anio5 = 0;
        $fc21->save();

        $fc22 = new FlujoCaja;
        $fc22->id_proyecto = $proyecto->id_proyecto;
        $fc22->ingresos = 'Amortización de intangibles (+)';
        $fc22->inicial = 0;
        $fc22->anio1 = 0;
        $fc22->anio2 = 0;
        $fc22->anio3 = 0;
        $fc22->anio4 = 0;
        $fc22->anio5 = 0;
        $fc22->save();

        $fc23 = new FlujoCaja;
        $fc23->id_proyecto = $proyecto->id_proyecto;
        $fc23->ingresos = 'Inversión inicial';
        $fc23->inicial = 0;
        $fc23->anio1 = 0;
        $fc23->anio2 = 0;
        $fc23->anio3 = 0;
        $fc23->anio4 = 0;
        $fc23->anio5 = 0;
        $fc23->save();

        $fc24 = new FlujoCaja;
        $fc24->id_proyecto = $proyecto->id_proyecto;
        $fc24->ingresos = 'FLUJO DE CAJA';
        $fc24->inicial = 0;
        $fc24->anio1 = 0;
        $fc24->anio2 = 0;
        $fc24->anio3 = 0;
        $fc24->anio4 = 0;
        $fc24->anio5 = 0;
        $fc24->save();
        */
        return Redirect::to('costos/materiaprima');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view("proyectos.proyecto.show",["proyecto" => Proyecto::findOrFail($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view("proyectos.proyecto.edit",["proyecto" => Proyecto::findOrFail($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProyectoFormRequest $request, $id)
    {
        $proyecto = Proyecto::findOrFail($id);
        $proyecto->nombre_proyecto = $request->get('nombre_proyecto');
        $proyecto->descripcion = $request->get('descripcion');
        $proyecto->update();
        return Redirect::to('proyectos/proyecto');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $proyecto = Proyecto::findOrFail($id);
        $proyecto->delete();
        return Redirect::to('proyectos/proyecto');
    }
}
