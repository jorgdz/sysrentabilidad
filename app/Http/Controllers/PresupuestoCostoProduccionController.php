<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\PresupuestoCostoProduccion;

use Illuminate\Support\Facades\Redirect;

use App\Http\Requests\PresupuestoCostoProduccionFormRequest;

use DB;

use Carbon\Carbon;

use Response;

use Illuminate\Support\Collection;

class PresupuestoCostoProduccionController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request) {
            $proyecto = DB::table('proyecto as p')->orderBy('p.id_proyecto','desc')->get();

            $lista_proyecto = $request->get('id_proyecto');

            $presupuesto = DB::table('presupuesto_costo_produccion as pre')
            ->join('proyecto as p', 'pre.id_proyecto', '=', 'p.id_proyecto')
            ->select('pre.id_presupuesto_costo_produccion','pre.concepto','pre.valor_anual')
            ->where('pre.id_proyecto','=',$lista_proyecto)
            ->orderBy('pre.id_presupuesto_costo_produccion','asc')
            ->groupBy('pre.id_presupuesto_costo_produccion','pre.concepto','pre.valor_anual')
            ->paginate(7);

            $totales_presupuesto = DB::table('presupuesto_costo_produccion as pre') 
            ->select(DB::raw('sum(pre.valor_anual) as total1'))
            ->where('pre.id_proyecto','=',$lista_proyecto)
            ->first();
            
            return view('produccion.presupuesto.index', ['presupuesto' => $presupuesto, 'proyecto' => $proyecto, 'totales_presupuesto' => $totales_presupuesto]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
