<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\GastosAdministracion;

use Illuminate\Support\Facades\Redirect;

use DB;

use Carbon\Carbon;

use Response;

use Illuminate\Support\Collection;

class GastosAdministracionController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request) {
            $proyecto = DB::table('proyecto as p')->orderBy('p.id_proyecto','desc')->get();

            $lista_proyecto = $request->get('id_proyecto');

            $gastos_admin = DB::table('gastos_administracion as ga')
            ->join('proyecto as p', 'ga.id_proyecto', '=', 'p.id_proyecto')
            ->select('ga.id_gastos_administracion', 'ga.concepto','ga.valor_anual','ga.presta_sociales', 'ga.sueldo_prestaciones')
            ->where('ga.id_proyecto','=',$lista_proyecto)
            ->orderBy('ga.id_gastos_administracion','asc')
            ->groupBy('ga.id_gastos_administracion', 'ga.concepto','ga.valor_anual','ga.presta_sociales', 'ga.sueldo_prestaciones')
            ->paginate(7);

            $total = DB::table('gastos_administracion as ga') 
            ->select(DB::raw('sum(ga.sueldo_prestaciones) as total'))
            ->where('ga.id_proyecto','=',$lista_proyecto)
            ->first();
            
            return view('gastos.administracion.index', ['gastos_admin' => $gastos_admin, 'proyecto' => $proyecto, 'total' => $total]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
