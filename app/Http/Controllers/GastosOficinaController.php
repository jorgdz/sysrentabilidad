<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\GastosOficina;

use App\Http\Requests\GastosOficinaFormRequest;

use Illuminate\Support\Facades\Redirect;

use DB;

use Carbon\Carbon;

use Response;

use Illuminate\Support\Collection;

class GastosOficinaController extends Controller
{   

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request) {
            $proyecto = DB::table('proyecto as p')->orderBy('p.id_proyecto','desc')->get();

            $lista_proyecto = $request->get('id_proyecto');

            $gastos_oficina = DB::table('gastos_oficina as go')
            ->join('proyecto as p', 'go.id_proyecto', '=', 'p.id_proyecto')
            ->select('go.id_gasto_oficina','go.concepto','go.cantidad_diaria', 'go.unidad', 'go.costo_unitario', 'go.costo_total_diario', 'go.costo_anual')
             ->where('go.id_proyecto','=',$lista_proyecto)
            ->orderBy('go.id_gasto_oficina','asc')
            ->groupBy('go.id_gasto_oficina','go.concepto','go.cantidad_diaria', 'go.unidad', 'go.costo_unitario', 'go.costo_total_diario', 'go.costo_anual')
            ->paginate(7);

            $total_gastos_oficina = DB::table('gastos_oficina as go') 
            ->select(DB::raw('sum(go.costo_anual) as total'))
            ->where('go.id_proyecto','=',$lista_proyecto)
            ->first();

            return view('gastos.oficina.index',['gastos_oficina' => $gastos_oficina, 'proyecto' => $proyecto, 'total_gastos_oficina' => $total_gastos_oficina]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $proyecto = DB::table('proyecto as p')->orderBy('p.id_proyecto','desc')->get();
        return view('gastos.oficina.create', ["proyecto" => $proyecto]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GastosOficinaFormRequest $request)
    {
        $go = new GastosOficina;
        $go->id_proyecto = $request->get('id_proyecto');
        $go->concepto = $request->get('concepto');
        $go->cantidad_diaria = $request->get('cantidad_diaria');
        $go->unidad = $request->get('unidad');
        $go->costo_unitario = $request->get('costo_unitario');
        $go->costo_total_diario = $request->get('cantidad_diaria') * $request->get('costo_unitario');
        $go->costo_anual = ($request->get('cantidad_diaria') * $request->get('costo_unitario')) * 12;
        $go->save();

        return Redirect::to('gastos/oficina');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $proyecto = DB::table('proyecto as p')->orderBy('p.id_proyecto','desc')->get();
        return view("gastos.oficina.edit",['gastos_oficina' => GastosOficina::findOrFail($id), 'proyecto' => $proyecto]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GastosOficinaFormRequest $request, $id)
    {
        $go = GastosOficina::findOrFail($id);
        $go->id_proyecto = $request->get('id_proyecto');
        $go->concepto = $request->get('concepto');
        $go->cantidad_diaria = $request->get('cantidad_diaria');
        $go->unidad = $request->get('unidad');
        $go->costo_unitario = $request->get('costo_unitario');
        $go->costo_total_diario = $request->get('cantidad_diaria') * $request->get('costo_unitario');
        $go->costo_anual = ($request->get('cantidad_diaria') * $request->get('costo_unitario')) * 12;
        $go->update();
        return Redirect::to('gastos/oficina');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
