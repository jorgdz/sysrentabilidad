<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\OtrosMateriales;

use Illuminate\Support\Facades\Redirect;

use App\Http\Requests\OtrosMaterialesFormRequest;

use DB;

use Carbon\Carbon;

use Response;

use Illuminate\Support\Collection;

class OtrosMaterialesController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request) {
            $proyecto = DB::table('proyecto as p')->orderBy('p.id_proyecto','desc')->get();

            $lista_proyecto = $request->get('id_proyecto');

            $otros_materiales = DB::table('otros_materiales as om')
            ->join('proyecto as p', 'om.id_proyecto', '=', 'p.id_proyecto')
            ->select('om.id_otros_materiales', 'om.concepto', 'om.cantidad', 'om.unidad', 'om.costo_unitario', 'om.costo_anual')
            ->where('om.id_proyecto','=',$lista_proyecto)
            ->orderBy('om.id_otros_materiales','asc')
            ->groupBy('om.id_otros_materiales', 'om.concepto', 'om.cantidad', 'om.unidad', 'om.costo_unitario', 'om.costo_anual')
            ->paginate(7);

            $totales_materia_prima = DB::table('materia_prima as mp') 
            ->select(DB::raw('sum(mp.demanda_anual) as total1'), DB::raw('sum(mp.costo_total_anual) as total2'))
            ->where('mp.id_proyecto','=',$lista_proyecto)
            ->first();

            $totales_otros_materiales = DB::table('otros_materiales as om') 
            ->select(DB::raw('sum(om.costo_anual) as total1'))
            ->where('om.id_proyecto','=',$lista_proyecto)
            ->first();

            return view('costos.materiales.index', ['otros_materiales' => $otros_materiales, 'proyecto' => $proyecto, 'total' => $totales_otros_materiales, 'totalmp' => $totales_materia_prima]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $proyecto = DB::table('proyecto as p')->orderBy('p.id_proyecto','desc')->get();
        return view('costos.materiales.create', ["proyecto" => $proyecto]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OtrosMaterialesFormRequest $request)
    {
        $om = new OtrosMateriales;
        $om->id_proyecto = $request->get('id_proyecto');
        $om->concepto = $request->get('concepto');
        $om->cantidad = $request->get('cantidad');
        $om->unidad = $request->get('unidad');
        $om->costo_unitario = $request->get('costo_unitario');        
        $om->costo_anual = $request->get('costo_unitario') * $request->get('cantidad');
        $om->save();

        return Redirect::to('costos/materiales');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $proyecto = DB::table('proyecto as p')->orderBy('p.id_proyecto','desc')->get();
        return view("costos.materiales.edit",['otros_materiales' => OtrosMateriales::findOrFail($id), 'proyecto' => $proyecto]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(OtrosMaterialesFormRequest $request, $id)
    {
        $om = OtrosMateriales::findOrFail($id);
        $om->id_proyecto = $request->get('id_proyecto');
        $om->concepto = $request->get('concepto');
        $om->cantidad = $request->get('cantidad');
        $om->unidad = $request->get('unidad');
        $om->costo_unitario = $request->get('costo_unitario');        
        $om->costo_anual = $request->get('costo_unitario') * $request->get('cantidad');
        $om->update();
        return Redirect::to('costos/materiales');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
