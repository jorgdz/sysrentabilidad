<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\ConsumoAgua;

use Illuminate\Support\Facades\Redirect;

use App\Http\Requests\ConsumoAguaFormRequest;

use DB;

use Response;

use Illuminate\Support\Collection;

class ConsumoAguaController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request) {
            $proyecto = DB::table('proyecto as p')->orderBy('p.id_proyecto','desc')->get();

            $lista_proyecto = $request->get('id_proyecto');

            $consumo_agua = DB::table('consumo_agua as ca')
            ->join('proyecto as p', 'ca.id_proyecto', '=', 'p.id_proyecto')
            ->select('ca.id_consumo_agua','ca.concepto','ca.cantidad', 'ca.consumo_anual', 'ca.costo_unitario', 'ca.costo_anual')
             ->where('ca.id_proyecto','=',$lista_proyecto)
            ->orderBy('ca.id_consumo_agua','asc')
            ->groupBy('ca.id_consumo_agua','ca.concepto','cantidad', 'consumo_anual', 'costo_unitario', 'costo_anual')
            ->paginate(7);

            $total_agua = DB::table('consumo_agua as ca')
            ->select(DB::raw('sum(ca.costo_anual) as total1'))
            ->where('ca.id_proyecto', '=', $lista_proyecto)
            ->first();

            $total = DB::table('materia_prima as mp') 
            ->select(DB::raw('sum(mp.demanda_anual) as total2'))
            ->where('mp.id_proyecto','=',$lista_proyecto)
            ->first();

            return view('consumos.agua.index',['consumo_agua' => $consumo_agua, 'proyecto' => $proyecto, 'total' => $total, 'total_agua' => $total_agua]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $proyecto = DB::table('proyecto as p')->orderBy('p.id_proyecto','desc')->get();
        return view("consumos.agua.edit",['agua' => ConsumoAgua::findOrFail($id), 'proyecto' => $proyecto]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ConsumoAguaFormRequest $request, $id)
    {   
        $ca = ConsumoAgua::findOrFail($id);
        $ca->cantidad = $request->get('cantidad');
        $ca->consumo_anual = $request->get('cantidad') * 288;
        $ca->costo_unitario = $request->get('costo_unitario');
        $ca->costo_anual = $ca->consumo_anual * $request->get('costo_unitario');
        $ca->update();
        return Redirect::to('consumos/agua');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
