<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Ingresos;

use Illuminate\Support\Facades\Redirect;

use App\Http\Requests\IngresosFormRequest;

use DB;

use Response;

use Illuminate\Support\Collection;

class IngresosController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request) {
            $proyecto = DB::table('proyecto as p')->orderBy('p.id_proyecto','desc')->get();

            $lista_proyecto = $request->get('id_proyecto');

            $ingresos = DB::table('ingresos as ing')
            ->join('proyecto as p', 'ing.id_proyecto', '=', 'p.id_proyecto')
            ->select('ing.id_ingreso', 'ing.concepto', 'ing.precio_costo', 'ing.cantidad_anual', 'ing.precio_unitario', 'ing.ingreso_anual')
             ->where('ing.id_proyecto','=',$lista_proyecto)
            ->orderBy('ing.id_ingreso','asc')
            ->groupBy('ing.id_ingreso', 'ing.concepto', 'ing.precio_costo', 'ing.cantidad_anual', 'ing.precio_unitario', 'ing.ingreso_anual')
            ->paginate(7);

            $total = DB::table('ingresos as ing')
            ->select(DB::raw('sum(ing.ingreso_anual) as total'))
            ->where('ing.id_proyecto', '=', $lista_proyecto)
            ->first();

            return view('ingresos.index',['total' => $total, 'proyecto' => $proyecto, 'ingresos' => $ingresos]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $proyecto = DB::table('proyecto as p')->orderBy('p.id_proyecto','desc')->get();
        return view("ingresos.edit",['ingresos' => Ingresos::findOrFail($id), 'proyecto' => $proyecto]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(IngresosFormRequest $request, $id)
    {
        $ing = Ingresos::findOrFail($id);
        $ing->precio_unitario = $request->get('precio_unitario');
        $ing->ingreso_anual = $ing->cantidad_anual * $request->get('precio_unitario');
        $ing->update();
        return Redirect::to('ingresos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
