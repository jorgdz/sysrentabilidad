<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\TotalGastosAdministracion;

use Illuminate\Support\Facades\Redirect;

use App\Http\Requests\TotalGastosAdministracionFormRequest;

use DB;

use Response;

use Illuminate\Support\Collection;

class TotalGastosAdministracionController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request) {
            $proyecto = DB::table('proyecto as p')->orderBy('p.id_proyecto','desc')->get();

            $lista_proyecto = $request->get('id_proyecto');

            $total_gasto_admin = DB::table('total_gastos_administracion as tga')
            ->join('proyecto as p', 'tga.id_proyecto', '=', 'p.id_proyecto')
            ->select('tga.id_gastos_administracion', 'tga.conceptos', 'tga.costos')
             ->where('tga.id_proyecto','=',$lista_proyecto)
            ->orderBy('tga.id_gastos_administracion','asc')
            ->groupBy('tga.id_gastos_administracion', 'tga.conceptos', 'tga.costos')
            ->paginate(7);

            $total_gasto_admin_costo = DB::table('total_gastos_administracion as tga')
            ->select(DB::raw('sum(tga.costos) as total'))
            ->where('tga.id_proyecto', '=', $lista_proyecto)
            ->first();

            return view('gastos.totales.index',['total_gasto_admin' => $total_gasto_admin, 'proyecto' => $proyecto, 'total_gasto_admin_costo' => $total_gasto_admin_costo]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TotalGastosAdministracionFormRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $proyecto = DB::table('proyecto as p')->orderBy('p.id_proyecto','desc')->get();
        return view("gastos.totales.edit",['total_gasto_admin' => TotalGastosAdministracion::findOrFail($id), 'proyecto' => $proyecto]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TotalGastosAdministracionFormRequest $request, $id)
    {
        $tga = TotalGastosAdministracion::findOrFail($id);
        $tga->costos = $request->get('costos');
        $tga->update();
        return Redirect::to('gastos/totales');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
