<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Prestaciones;

use Illuminate\Support\Facades\Redirect;

use App\Http\Requests\PrestacionesFormRequest;

use DB;

use Carbon\Carbon;

use Response;

use Illuminate\Support\Collection;

class PrestacionesController extends Controller
{   

    public function __construct(){
        $this->middleware('auth');
    } 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request) {
            $proyecto = DB::table('proyecto as p')->orderBy('p.id_proyecto','desc')->get();

            $lista_proyecto = $request->get('id_proyecto');

            $prestacion = DB::table('prestaciones as pres')
            ->join('proyecto as p', 'pres.id_proyecto', '=', 'p.id_proyecto')
            ->select('pres.id_prestaciones','pres.nomina','pres.numero','pres.sueldo_mensual', 'pres.horas_suplementarias', 'sueldo_anual', 'pres.treceavo_sueldo', 'pres.catorceavo_sueldo', 'pres.fondo_reserva', 'pres.vacaciones', 'pres.aporte_iess','pres.total_prestaciones_primer_aniol')
             ->where('pres.id_proyecto','=',$lista_proyecto)
            ->orderBy('id_prestaciones','asc')
            ->groupBy('pres.id_prestaciones','pres.nomina','pres.numero','pres.sueldo_mensual', 'pres.horas_suplementarias', 'sueldo_anual', 'pres.treceavo_sueldo', 'pres.catorceavo_sueldo', 'pres.fondo_reserva', 'pres.vacaciones', 'pres.aporte_iess','pres.total_prestaciones_primer_aniol')
            ->paginate(7);

            $totales = DB::table('prestaciones as pres') 
            ->select(DB::raw('sum(pres.numero) as total1'), DB::raw('sum(pres.sueldo_mensual) as total2'), DB::raw('sum(pres.horas_suplementarias) as total3'), DB::raw('sum(pres.sueldo_anual) as total4'), DB::raw('sum(pres.treceavo_sueldo) as total5'), DB::raw('sum(pres.catorceavo_sueldo) as total6'), DB::raw('sum(pres.fondo_reserva) as total7'),DB::raw('sum(pres.vacaciones) as total8'), DB::raw('sum(pres.aporte_iess) as total9'), DB::raw('sum(pres.total_prestaciones_primer_aniol) as total10'))
            ->where('pres.id_proyecto','=',$lista_proyecto)
            ->first();

            return view('costos.prestaciones.index',['prestacion' => $prestacion, 'proyecto' => $proyecto, 'totales' => $totales]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $proyecto = DB::table('proyecto as p')->orderBy('p.id_proyecto','desc')->get();
        return view("costos.prestaciones.edit",['prestacion' => Prestaciones::findOrFail($id), 'proyecto' => $proyecto]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PrestacionesFormRequest $request, $id)
    {
        $p = Prestaciones::findOrFail($id);
        $p->numero = $request->get('numero');
        $p->sueldo_mensual = $request->get('sueldo_mensual');
        $p->horas_suplementarias = $request->get('horas_suplementarias');  
        $sueldito_anual = ($request->get('sueldo_mensual') * 12) + $request->get('horas_suplementarias');
        $p->sueldo_anual = $sueldito_anual;               
        $p->treceavo_sueldo = $request->get('treceavo_sueldo');
        $p->catorceavo_sueldo = $request->get('catorceavo_sueldo');
        $p->fondo_reserva = $request->get('fondo_reserva');
        $p->vacaciones = $request->get('vacaciones');
        $aportIess = (11.15/100) * $sueldito_anual;
        $p->aporte_iess = $aportIess;        
        $p->total_prestaciones_primer_aniol = $request->get('treceavo_sueldo') + $request->get('catorceavo_sueldo') + $request->get('fondo_reserva') + $request->get('vacaciones') + $aportIess;
        $p->update();
        return Redirect::to('costos/prestaciones');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
