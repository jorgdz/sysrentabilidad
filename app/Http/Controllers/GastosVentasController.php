<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\TotalGastosVenta;

use App\Http\Requests\TotalGastosVentaFormRequest;

use Illuminate\Support\Facades\Redirect;

use DB;

use Carbon\Carbon;

use Response;

use Illuminate\Support\Collection;

class GastosVentasController extends Controller
{   

    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request) {
            $proyecto = DB::table('proyecto as p')->orderBy('p.id_proyecto','desc')->get();

            $lista_proyecto = $request->get('id_proyecto');

            $gastos_ventas = DB::table('total_gastos_venta as tgv')
            ->join('proyecto as p', 'tgv.id_proyecto', '=', 'p.id_proyecto')
            ->select('tgv.id_gastos_venta', 'tgv.concepto', 'tgv.costo')
             ->where('tgv.id_proyecto','=',$lista_proyecto)
            ->orderBy('tgv.id_gastos_venta','asc')
            ->groupBy('tgv.id_gastos_venta', 'tgv.concepto', 'tgv.costo')
            ->paginate(7);

            $total_gastos_ventas = DB::table('total_gastos_venta as tgv') 
            ->select(DB::raw('sum(tgv.costo) as total'))
            ->where('tgv.id_proyecto','=',$lista_proyecto)
            ->first();

            return view('gastos.ventas.index',['gastos_ventas' => $gastos_ventas, 'proyecto' => $proyecto, 'total_gastos_ventas' => $total_gastos_ventas]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $proyecto = DB::table('proyecto as p')->orderBy('p.id_proyecto','desc')->get();
        return view('gastos.ventas.create', ["proyecto" => $proyecto]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TotalGastosVentaFormRequest $request)
    {
        $tgv = new TotalGastosVenta;
        $tgv->id_proyecto = $request->get('id_proyecto');
        $tgv->concepto = $request->get('concepto');
        $tgv->costo = $request->get('costo');
        $tgv->save();
        return Redirect::to('gastos/ventas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $proyecto = DB::table('proyecto as p')->orderBy('p.id_proyecto','desc')->get();
        return view("gastos.ventas.edit",['gastos_ventas' => TotalGastosVenta::findOrFail($id), 'proyecto' => $proyecto]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TotalGastosVentaFormRequest $request, $id)
    {
        $tgv = TotalGastosVenta::findOrFail($id);
        $tgv->id_proyecto = $request->get('id_proyecto');
        $tgv->concepto = $request->get('concepto');
        $tgv->costo = $request->get('costo');
        $tgv->update();
        return Redirect::to('gastos/ventas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
