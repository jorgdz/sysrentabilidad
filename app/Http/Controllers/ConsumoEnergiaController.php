<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\ConsumoEnergia;

use Illuminate\Support\Facades\Redirect;

use App\Http\Requests\ConsumoEnergiaFormRequest;

use DB;

class ConsumoEnergiaController extends Controller
{   

    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request) {
            $proyecto = DB::table('proyecto as p')->orderBy('p.id_proyecto','desc')->get();

            $lista_proyecto = $request->get('id_proyecto');

            $consumo_energia = DB::table('consumo_energia as ce')
            ->join('proyecto as p', 'ce.id_proyecto', '=', 'p.id_proyecto')
            ->select('ce.id_consumo_energia','ce.descripcion','ce.consumo_mensual', 'ce.consumo_anual')
             ->where('ce.id_proyecto','=',$lista_proyecto)
            ->orderBy('ce.id_consumo_energia','asc')
            ->groupBy('ce.id_consumo_energia','ce.descripcion','ce.consumo_mensual', 'ce.consumo_anual')
            ->paginate(7);

            $total_energia = DB::table('consumo_energia as ce')
            ->select(DB::raw('sum(ce.consumo_anual) as total1'))
            ->where('ce.id_proyecto', '=', $lista_proyecto)
            ->first();

            $total = DB::table('materia_prima as mp') 
            ->select(DB::raw('sum(mp.demanda_anual) as total2'))
            ->where('mp.id_proyecto','=',$lista_proyecto)
            ->first();

            return view('consumos.energia.index',['consumo_energia' => $consumo_energia, 'proyecto' => $proyecto, 'total' => $total, 'total_energia' => $total_energia]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $proyecto = DB::table('proyecto as p')->orderBy('p.id_proyecto','desc')->get();
        return view("consumos.energia.edit",['energia' => ConsumoEnergia::findOrFail($id), 'proyecto' => $proyecto]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ConsumoEnergiaFormRequest $request, $id)
    {
        $ce = ConsumoEnergia::findOrFail($id);
        $ce->consumo_mensual = $request->get('consumo_mensual');
        $ce->consumo_anual = $request->get('consumo_mensual') * 12;
        $ce->update();
        return Redirect::to('consumos/energia');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
