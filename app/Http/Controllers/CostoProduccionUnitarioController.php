<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\CostoProduccionUnitario;

use Illuminate\Support\Facades\Redirect;

use App\Http\Requests\CostoProduccionUnitarioFormRequest;

use DB;

use Carbon\Carbon;

use Response;

use Illuminate\Support\Collection;

class CostoProduccionUnitarioController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request) {
            $proyecto = DB::table('proyecto as p')->orderBy('p.id_proyecto','desc')->get();

            $lista_proyecto = $request->get('id_proyecto');

            $costoprodu = DB::table('costo_produccion_unitario as cpu')
            ->join('proyecto as p', 'cpu.id_proyecto', '=', 'p.id_proyecto')
            ->select('id_costo_produccion_unitario', 'conceptos', 'valor_unitario')
            ->where('cpu.id_proyecto','=',$lista_proyecto)
            ->orderBy('cpu.id_costo_produccion_unitario','asc')
            ->groupBy('id_costo_produccion_unitario', 'conceptos', 'valor_unitario')
            ->paginate(9);

            $total = DB::table('costo_produccion_unitario as cpu') 
            ->select(DB::raw('sum(cpu.valor_unitario) as total'))
            ->where('cpu.id_proyecto','=',$lista_proyecto)
            ->first();
            
            return view('produccion.unitaria.index', ['costoprodu' => $costoprodu, 'proyecto' => $proyecto, 'total' => $total]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
