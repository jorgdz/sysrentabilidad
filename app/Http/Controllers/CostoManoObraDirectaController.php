<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\CostoManoObraDirecta;

use Illuminate\Support\Facades\Redirect;

use App\Http\Requests\CostoManoObraDirectaFormRequest;

use DB;

use Carbon\Carbon;

use Response;

use Illuminate\Support\Collection;

class CostoManoObraDirectaController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    } 
 

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request) {
            $proyecto = DB::table('proyecto as p')->orderBy('p.id_proyecto','desc')->get();

            $lista_proyecto = $request->get('id_proyecto');

            $costomanoobra = DB::table('costo_mano_obra_directa as c')
            ->join('proyecto as p', 'c.id_proyecto', '=', 'p.id_proyecto')
            ->select('c.id_costo_mano_obra_directa','c.personal', 'c.cantidad', 'c.remuneracion', 'c.monto_total')
             ->where('c.id_proyecto','=',$lista_proyecto)
            ->orderBy('c.id_costo_mano_obra_directa','asc')
            ->groupBy('c.id_costo_mano_obra_directa','c.personal', 'c.cantidad', 'c.remuneracion', 'c.monto_total')
            ->paginate(7);

            $total_mano_obra = DB::table('costo_mano_obra_directa as c') 
            ->select(DB::raw('sum(c.monto_total) as total1'))
            ->where('c.id_proyecto','=',$lista_proyecto)
            ->first();

            $totales_materia_prima = DB::table('materia_prima as mp') 
            ->select(DB::raw('sum(mp.demanda_anual) as total2'))
            ->first();

            return view('costos.manoobra.index',['costomanoobra' => $costomanoobra, 'proyecto' => $proyecto, 'total_mano_obra' => $total_mano_obra, 'totales_materia_prima' => $totales_materia_prima]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $proyecto = DB::table('proyecto as p')->orderBy('p.id_proyecto','desc')->get();
        return view('costos.manoobra.create', ["proyecto" => $proyecto]);        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CostoManoObraDirectaFormRequest $request)
    {
        $c = new CostoManoObraDirecta;
        $c->id_proyecto = $request->get('id_proyecto');
        $c->personal = $request->get('personal');
        $c->cantidad = $request->get('cantidad');
        $c->remuneracion = $request->get('remuneracion');
        $c->monto_total = $request->get('cantidad') * $request->get('remuneracion');
        $c->save();

        return Redirect::to('costos/manoobra');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $proyecto = DB::table('proyecto as p')->orderBy('p.id_proyecto','desc')->get();
        return view("costos.manoobra.edit",['costomanoobra' => CostoManoObraDirecta::findOrFail($id), 'proyecto' => $proyecto]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CostoManoObraDirectaFormRequest $request, $id)
    {
        $c = CostoManoObraDirecta::findOrFail($id);
        $c->id_proyecto = $request->get('id_proyecto');
        $c->personal = $request->get('personal');
        $c->cantidad = $request->get('cantidad');
        $c->remuneracion = $request->get('remuneracion');
        $c->monto_total = $request->get('cantidad') * $request->get('remuneracion');
        $c->update();
        return Redirect::to('costos/manoobra');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
