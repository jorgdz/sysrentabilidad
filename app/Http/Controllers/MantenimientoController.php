<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Mantenimiento;

use Illuminate\Support\Facades\Redirect;

use App\Http\Requests\MantenimientoFormRequest;

use DB;

use Carbon\Carbon;

use Response;

use Illuminate\Support\Collection;


class MantenimientoController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request) {
            $proyecto = DB::table('proyecto as p')->orderBy('p.id_proyecto','desc')->get();

            $lista_proyecto = $request->get('id_proyecto');

            $mantenimiento = DB::table('mantenimiento as m')
            ->join('proyecto as p', 'm.id_proyecto', '=', 'p.id_proyecto')
            ->select('m.id_mantenimiento', 'm.id_proyecto','m.descripcion','m.cantidad','m.costo_anual')
            ->where('m.id_proyecto','=',$lista_proyecto)
            ->orderBy('m.id_mantenimiento','asc')
            ->groupBy('m.id_mantenimiento', 'm.id_proyecto','m.descripcion','m.cantidad','m.costo_anual')
            ->paginate(7);

            $totales_materia_prima = DB::table('materia_prima as mp') 
            ->select(DB::raw('sum(mp.demanda_anual) as total1'), DB::raw('sum(mp.costo_total_anual) as total2'))
            ->where('mp.id_proyecto','=',$lista_proyecto)
            ->first();

            $totales_mantenimiento = DB::table('mantenimiento as m') 
            ->select(DB::raw('sum(m.costo_anual) as total1'))
            ->where('m.id_proyecto','=',$lista_proyecto)
            ->first();

            return view('costos.mantenimiento.index', ['mantenimiento' => $mantenimiento, 'proyecto' => $proyecto, 'total' => $totales_mantenimiento, 'totalmp' => $totales_materia_prima]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $proyecto = DB::table('proyecto as p')->orderBy('p.id_proyecto','desc')->get();
        return view('costos.mantenimiento.create', ["proyecto" => $proyecto]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MantenimientoFormRequest $request)
    {
        $m = new Mantenimiento;
        $m->id_proyecto = $request->get('id_proyecto');
        $m->descripcion = $request->get('descripcion');
        $m->cantidad = $request->get('cantidad');
        $m->costo_anual = $request->get('cantidad') * 4;
        $m->save();

        return Redirect::to('costos/mantenimiento'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $proyecto = DB::table('proyecto as p')->orderBy('p.id_proyecto','desc')->get();
        return view("costos.mantenimiento.edit",['mantenimiento' => Mantenimiento::findOrFail($id), 'proyecto' => $proyecto]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MantenimientoFormRequest $request, $id)
    {
        $m = Mantenimiento::findOrFail($id);
        $m->id_proyecto = $request->get('id_proyecto');
        $m->descripcion = $request->get('descripcion');
        $m->cantidad = $request->get('cantidad');
        $m->costo_anual = $request->get('cantidad') * 4;
        $m->update();
        return Redirect::to('costos/mantenimiento');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
