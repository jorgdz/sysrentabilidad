<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\GastosGenerales;

use App\Http\Requests\GastosGeneralesFormRequest;

use Illuminate\Support\Facades\Redirect;

use DB;

use Carbon\Carbon;

use Response;

use Illuminate\Support\Collection;

class GastosGeneralesController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }        
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request) {
            $proyecto = DB::table('proyecto as p')->orderBy('p.id_proyecto','desc')->get();

            $lista_proyecto = $request->get('id_proyecto');

            $gastos_generales = DB::table('gastos_generales as g')
            ->join('proyecto as p', 'g.id_proyecto', '=', 'p.id_proyecto')
            ->select('g.id_gasto_general','g.concepto', 'g.valor_mensual', 'g.costo_anual')
             ->where('g.id_proyecto','=',$lista_proyecto)
            ->orderBy('g.id_gasto_general','asc')
            ->groupBy('g.id_gasto_general','g.concepto', 'g.valor_mensual', 'g.costo_anual')
            ->paginate(7);

            $total_gastos_generales = DB::table('gastos_generales as g') 
            ->select(DB::raw('sum(g.costo_anual) as total'))
            ->where('g.id_proyecto','=',$lista_proyecto)
            ->first();

            return view('gastos.generales.index',['gastos_generales' => $gastos_generales, 'proyecto' => $proyecto, 'total_gastos_generales' => $total_gastos_generales]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $proyecto = DB::table('proyecto as p')->orderBy('p.id_proyecto','desc')->get();
        return view('gastos.generales.create', ["proyecto" => $proyecto]);  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GastosGeneralesFormRequest $request)
    {
        $gg = new GastosGenerales;
        $gg->id_proyecto = $request->get('id_proyecto');
        $gg->concepto = $request->get('concepto');
        $gg->valor_mensual = $request->get('valor_mensual');
        $gg->costo_anual = $request->get('valor_mensual') * 12;
        $gg->save();

        return Redirect::to('gastos/generales');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $proyecto = DB::table('proyecto as p')->orderBy('p.id_proyecto','desc')->get();
        return view("gastos.generales.edit",['gastos_generales' => GastosGenerales::findOrFail($id), 'proyecto' => $proyecto]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GastosGeneralesFormRequest $request, $id)
    {
        $gg = GastosGenerales::findOrFail($id);
        $gg->id_proyecto = $request->get('id_proyecto');
        $gg->concepto = $request->get('concepto');
        $gg->valor_mensual = $request->get('valor_mensual');
        $gg->costo_anual = $request->get('valor_mensual') * 12;
        $gg->update();
        return Redirect::to('gastos/generales');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
