<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\FlujoCaja;

use Illuminate\Support\Facades\Redirect;

use App\Http\Requests\FlujoCajaFormRequest;

use DB;

use Carbon\Carbon;

use Response;

use Illuminate\Support\Collection;

class FlujoCajaController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request) {
            $proyecto = DB::table('proyecto as p')->orderBy('p.id_proyecto','desc')->get();

            $lista_proyecto = $request->get('id_proyecto');

            $flujo = DB::table('flujo_caja as fc')
            ->join('proyecto as p', 'fc.id_proyecto', '=', 'p.id_proyecto')
            ->select('fc.id_flujo_caja', 'fc.ingresos', 'fc.inicial', 'fc.anio1', 'fc.anio2', 'fc.anio3', 'fc.anio4', 'fc.anio5')
            ->where('fc.id_proyecto','=',$lista_proyecto)
            ->orderBy('fc.id_flujo_caja','asc')
            ->groupBy('fc.id_flujo_caja', 'fc.ingresos', 'fc.inicial', 'fc.anio1', 'fc.anio2', 'fc.anio3', 'fc.anio4', 'fc.anio5')
            ->paginate(26);
            
            $fc_total_egreso_anio = DB::table('flujo_caja as fc') 
            ->select(DB::raw('sum(fc.anio1) as total1'), DB::raw('sum(fc.anio2) as total2'), DB::raw('sum(fc.anio3) as total3'), DB::raw('sum(fc.anio4) as total4'), DB::raw('sum(fc.anio5) as total5'))
            ->where('fc.id_proyecto','=',$lista_proyecto)
            ->where('fc.ingresos', '<>', 'INGRESOS TOTALES')
            ->where('fc.ingresos', '<>', 'Préstamo')
            ->where('fc.ingresos', '<>', 'Ventas')
            ->first();

            $fc_inversion = DB::table('flujo_caja as fc') 
            ->select(DB::raw('sum(fc.inicial) as total_inicial'))
            ->where('fc.id_proyecto','=',$lista_proyecto)
            ->where('fc.ingresos', '=', 'Préstamo')
            ->first();

            $van = ($fc_inversion->total_inicial * -1) + ( ($fc_total_egreso_anio->total1 / (1+0.098) ) + ($fc_total_egreso_anio->total2 / (pow((1+0.098), 2)) ) + ($fc_total_egreso_anio->total3 / (pow((1+0.098), 3)) ) + ($fc_total_egreso_anio->total4 / (pow((1+0.098), 4)) ) + ($fc_total_egreso_anio->total5 / (pow((1+0.098), 5)) ) );

            return view('flujo.caja.index', ['flujo' => $flujo, 'proyecto' => $proyecto, 'fc_total_egreso_anio' => $fc_total_egreso_anio, 'fc_inversion' => $fc_inversion, 'van' => $van]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $proyecto = DB::table('proyecto as p')->orderBy('p.id_proyecto','desc')->get();
        return view("flujo.caja.edit",['flujo' => FlujoCaja::findOrFail($id), 'proyecto' => $proyecto]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FlujoCajaFormRequest $request, $id)
    {
        $fc = FlujoCaja::findOrFail($id);
        
        $fc->inicial = $request->get('inicial'); 
        $fc->anio1 = $request->get('anio1');
        $fc->anio2 = $request->get('anio2');
        $fc->anio3 = $request->get('anio3');
        $fc->anio4 = $request->get('anio4');
        $fc->anio5 = $request->get('anio5');               
        $fc->update();

        $fc2 = DB::table('flujo_caja as fc')->where('fc.ingresos','=','INGRESOS TOTALES')->first();
        $fc3 = FlujoCaja::findOrFail($fc2->id_flujo_caja); 
        $fc3->inicial = $fc->inicial;
        $fc3->update();

        return Redirect::to('flujo/caja');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
