<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CostoManoObraDirecta extends Model
{
    protected $table = 'costo_mano_obra_directa';
    protected $primaryKey = 'id_costo_mano_obra_directa';
    public $timestamps = false;

    protected $fillable = [
		'id_proyecto',
        'personal',
        'cantidad',
        'remuneracion',
        'monto_total'
    ];
}
