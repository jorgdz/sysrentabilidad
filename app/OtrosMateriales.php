<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OtrosMateriales extends Model
{
    protected $table = 'otros_materiales';
    protected $primaryKey = 'id_otros_materiales';
    public $timestamps = false;

    protected $fillable = [
		'id_proyecto',
  		'concepto',
	    'cantidad', 
		'unidad',
		'costo_unitario',
		'costo_anual'
    ];
}
