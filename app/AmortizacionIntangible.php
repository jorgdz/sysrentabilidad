<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AmortizacionIntangible extends Model
{
    protected $table = 'amortizacion_intangible';
    protected $primaryKey = 'id_amortizacion_intangible';
    public $timestamps = false;

    protected $fillable = [
		  'id_proyecto',
		  'concepto',
		  'valor',
		  'porcentaje',
		  'anio1',
		  'anio2',
		  'anio3',
		  'anio4',
		  'anio5'
    ];
}
