<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GastosOficina extends Model
{
    protected $table = 'gastos_oficina';
    protected $primaryKey = 'id_gasto_oficina';
    public $timestamps = false;

    protected $fillable = [
		  'id_proyecto',
  		'concepto',
  		'cantidad_diaria',
  		'unidad',
  		'costo_unitario',
  		'costo_total_diario',
  		'costo_anual'
    ];
}
